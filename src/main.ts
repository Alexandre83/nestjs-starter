import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import cors from 'cors';
import swStats from 'swagger-stats';

import { AppModule } from './app.module';
import { configService } from './common/config/config.service';
import { LoggingInterceptor } from './common/interceptor/logging.interceptor';
import { ApiLogger } from './common/provider/logger/api.logger';

async function bootstrap() {
  const version = '0.1';
  const env = configService.getString('NODE_ENV');
  const logger = env === 'production' ? new ApiLogger() : new Logger();
  const app = await NestFactory.create(AppModule, { logger });

  app.use(cors());
  app.use(
    swStats.getMiddleware({
      uriPath: '/stats',
      version,
    }),
  );

  const options = new DocumentBuilder()
    .setTitle('API')
    .setVersion(`${version}`)
    .addBearerAuth('Authorization', 'header')
    .setSchemes(env === 'production' ? 'https' : 'http')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/docs', app, document, { customSiteTitle: 'API', swaggerOptions: { docExpansion: 'none' } });

  app.useGlobalInterceptors(new LoggingInterceptor(logger));

  await app.listen(configService.getNumber('PORT'));
}
bootstrap();
