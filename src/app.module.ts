import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './common/config/config.module';
import { HooksModule } from './common/provider/hooks/hooks.module';

@Module({
  imports: [ConfigModule, HooksModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
