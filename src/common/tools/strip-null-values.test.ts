import { stripNullValues } from './strip-null-values';

describe('stripNullValues', () => {
  it('should strips `null` values properties', async () => {
    const src = {
      id: '123',
      name: null,
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual({ id: '123' });
  });

  it('should not strips Date values properties', async () => {
    const src = {
      id: '123',
      created: new Date(),
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual(src);
  });

  it('should not strips array values properties within array of string values', async () => {
    const src = {
      obj: [['123', '456'], ['1337']],
      created: new Date(),
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual(src);
  });

  it('should not strips string empty value (falsy) properties', async () => {
    const src = {
      id: '123',
      name: '',
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual({ id: '123', name: '' });
  });

  it('should not strips boolean false value (falsy) properties', async () => {
    const src = {
      id: '123',
      isActive: false,
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual({ id: '123', isActive: false });
  });

  it('should not strips number 0 value (falsy) properties', async () => {
    const src = {
      id: '123',
      nbWorkers: 0,
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual({ id: '123', nbWorkers: 0 });
  });

  it('should strips null value (falsy) properties within nested object', async () => {
    const src = {
      id: '123',
      nbWorkers: 0,
      nestedLambda: {
        name: null,
      },
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual({ id: '123', nbWorkers: 0, nestedLambda: {} });
  });

  it('should strips null value (falsy) properties within array of nested object', async () => {
    const src = {
      id: '123',
      nbWorkers: 0,
      nestedLambda: [
        {
          name: null,
        },
        {
          name: 'Michel',
        },
      ],
    };
    const instance = await stripNullValues(src);
    expect(instance).toEqual({
      id: '123',
      nbWorkers: 0,
      nestedLambda: [
        {},
        {
          name: 'Michel',
        },
      ],
    });
  });

  it('should strips null value (falsy) properties within array nested values', async () => {
    const src = [
      {
        id: '123',
        nbWorkers: 42,
        nestedLambda: [
          {
            name: 'Jean',
          },
        ],
      },
      {
        id: '456',
        nbWorkers: null,
        nestedLambda: [
          {
            name: null,
          },
          {
            name: 'Michel',
          },
        ],
      },
    ];
    const instance = await stripNullValues(src as any);
    expect(instance).toEqual([
      {
        id: '123',
        nbWorkers: 42,
        nestedLambda: [
          {
            name: 'Jean',
          },
        ],
      },
      {
        id: '456',
        nestedLambda: [
          {},
          {
            name: 'Michel',
          },
        ],
      },
    ]);
  });

  it('should not strips values (falsy) properties within array string values', async () => {
    const src = [{ ids: ['1234', '567'] }, { ids: ['1337'] }];

    const instance = await stripNullValues(src as any);

    expect(instance).toEqual([{ ids: ['1234', '567'] }, { ids: ['1337'] }]);
  });
});
