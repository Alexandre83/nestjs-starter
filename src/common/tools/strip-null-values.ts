/**
 * Strip null values from source object or array of object (including within nested and array of object)
 *
 * @param src - Object or array of object to strip null values
 *
 * @return return object or array of object with stripped null values (including nested object)
 */
export function stripNullValues(src: any): any {
  const result: any = {};

  if (typeof src !== 'object' || src instanceof Date) {
    return src;
  } else if (Array.isArray(src)) {
    return src.map(item => stripNullValues(item));
  }

  Object.entries(src).forEach(([key, value]) => {
    if (value !== null) {
      if (Array.isArray(value)) {
        result[key] = value.map(item => stripNullValues(item));
      } else if (typeof value === 'object') {
        result[key] = stripNullValues(value);
      } else {
        result[key] = value;
      }
    }
  });
  return result;
}
