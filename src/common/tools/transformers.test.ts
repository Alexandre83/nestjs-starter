import { arrayEnumStringTransformer, dateTransformer, enumNumericTransformer, numericNumberTransformer } from './transformers';

describe('numericNumberTransformer', () => {
  it('"to" database function convert number to string', () => {
    const numberData = 1337.4242;

    expect(numericNumberTransformer.to(numberData)).toEqual('1337.4242');
  });

  it('"from" database function convert string to number', () => {
    const stringData = '1337.4242';

    expect(numericNumberTransformer.from(stringData)).toEqual(1337.4242);
  });
});

describe('enumNumericTransformer', () => {
  it('"to" database function return the value', () => {
    const numberData = 42;

    expect(enumNumericTransformer.to(numberData)).toEqual(numberData);
  });

  it('"from" database function convert string to number', () => {
    const stringData = '42';

    expect(enumNumericTransformer.from(stringData)).toEqual(42);
  });

  it('"from" database function return no value when value is undefined', () => {
    expect(enumNumericTransformer.from(undefined as any)).toEqual(undefined);
  });

  it('"from" database function return no value when value is empty string (falsy value)', () => {
    const stringData = '';

    expect(enumNumericTransformer.from(stringData)).toEqual(undefined);
  });

  it('"from" database function convert array of string to array of number', () => {
    const valueDbData: string[] = ['42', '1337'];

    expect(enumNumericTransformer.from(valueDbData)).toEqual([42, 1337]);
  });

  it('"from" database function return empty values value when values is empty', () => {
    const valueDbData: string[] = [];

    expect(enumNumericTransformer.from(valueDbData)).toEqual([]);
  });

  it('"from" database function return empty values value when values has falsy value', () => {
    const valueDbData: string[] = [''];

    expect(enumNumericTransformer.from(valueDbData)).toEqual([]);
  });
});

describe('arrayEnumStringTransformer', () => {
  it('"to" database function return the value', () => {
    const arrayEnumData = ['ENUM_VALUE'];

    expect(arrayEnumStringTransformer.to(arrayEnumData)).toEqual(arrayEnumData);
  });

  it('"from" database function return an array of enum with one value', () => {
    const arrayEnumData = ['ENUM_VALUE'];

    expect(arrayEnumStringTransformer.from(arrayEnumData)).toEqual(arrayEnumData);
  });

  it('"from" database function return an array of enum with two values', () => {
    const arrayEnumData = ['ENUM_VALUE', 'ENUM_VALUE2'];

    expect(arrayEnumStringTransformer.from(arrayEnumData)).toEqual(arrayEnumData);
  });

  it('"from" database function return no value when value is undefined', () => {
    expect(arrayEnumStringTransformer.from(undefined as any)).toEqual(undefined);
  });

  it('"from" database function return the value when the array is empty from PostgeSQL', () => {
    const arrayEnumData: string[] = [];

    expect(arrayEnumStringTransformer.from(arrayEnumData)).toEqual(arrayEnumData);
  });

  it('"from" database function return empty values value when values has falsy value', () => {
    const valueDbData = [''];

    expect(enumNumericTransformer.from(valueDbData)).toEqual([]);
  });
});

describe('dateTransformer', () => {
  it('"to" database function return the value', () => {
    const data = new Date('2018-09-10');

    expect(dateTransformer.to(data)).toBe(data);
  });

  describe('"from" database function return the Date Primitive corresponding to the db date', () => {
    it('handles YYYY-MM-DD format', () => {
      const date = '2018-09-10';

      const transformedDate = dateTransformer.from(date);

      expect(transformedDate).toBeInstanceOf(Date);
      expect(JSON.stringify(transformedDate)).toEqual(JSON.stringify(new Date(date)));
    });

    it('handles YYYY-MM-DD HH:ZZ format', () => {
      const date = '2018-09-10 22:12';

      const transformedDate = dateTransformer.from(date);

      expect(transformedDate).toBeInstanceOf(Date);
      expect(JSON.stringify(transformedDate)).toEqual(JSON.stringify(new Date(date)));
    });

    it('handles iso format', () => {
      const date = new Date('2018-09-10 22:12').toISOString();

      const transformedDate = dateTransformer.from(date);

      expect(transformedDate).toBeInstanceOf(Date);
      expect(JSON.stringify(transformedDate)).toEqual(JSON.stringify(new Date(date)));
    });
  });
});
