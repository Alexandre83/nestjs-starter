/**
 * TypeORM transformer to convert `numeric` PostgeSQL type to number TypeScript type
 * @type {{to: (value: number) => string; from: (value: string) => number}}
 */
export const numericNumberTransformer = {
  from: (value: string) => parseFloat(value),
  to: (value?: number) => (value ? value.toString() : undefined),
};

/**
 * TypeORM transformer to convert `enum` in `numeric` format to enum `string` format within PostgeSQL
 * Accept also `array` of `enum`
 *
 * @type {{from: (value: string | string[]) => number | number[], to: (value: number | number[]) => string | string[]}}
 */
export const enumNumericTransformer = {
  from: (value: string | string[]) => {
    if (value) {
      if (Array.isArray(value)) {
        // If it's an array, remove all falsy values
        return value.filter(v => v).map(v => parseFloat(v));
      }
      return parseFloat(value);
    }
  },
  to: (value: number | number[]) => value,
};

/**
 * TypeORM transformer fix the output of TypeORM when an empty enum array from PostgeSQL is retrieved
 * Will be remove this transformer when this issue: https://github.com/typeorm/typeorm/issues/2871 will be resolved
 *
 * @type {{from: (value: string[]) => string[], to: (value: string[]) => string[]}}
 */
export const arrayEnumStringTransformer = {
  from: (value: string[]) => {
    if (value && value.length === 1 && value[0] === '') {
      return [];
    }
    return value;
  },
  to: (value: string[]) => value,
};

/**
 * TypeORM transformer to get an instance of Date Native Object instead of Date string
 */
export const dateTransformer = {
  from: (value: string) => new Date(value),
  to: (value: Date | string) => value,
};
