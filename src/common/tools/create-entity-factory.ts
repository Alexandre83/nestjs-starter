import { Type } from '@nestjs/common';
import _ from 'lodash';
import { getManager } from 'typeorm';

/**
 * Test Entities Factory
 *
 * Creates two functions allowing to create test data with modularity
 *
 * @see devdocs test-fixtures
 * @template Entity The Entity to create functions for
 * @param {Type<Entity>} entity Class of the target Entity
 * @param {() => Partial<Entity>} [generator=() => ({})] function returning the fixture data for a instance of the given entity
 * @returns createOne, createMany
 */
export function createEntityFactory<Entity>(
  entity: Type<Entity>,
  generator: (saveInDb: boolean, override: Partial<Entity>) => Partial<Entity> | Promise<Partial<Entity>> = () => ({}),
) {
  /**
   * Creates a single instance of an entity
   *
   * @see devdocs test-fixtures
   * @param {boolean} saveInDb - whether or not save the created entity in db
   * @param {Partial<Entity>} override - object allowing to customize the created object
   * @returns {Promise<Entity>} Resolves with the entity created
   */
  async function createOne(saveInDb: boolean, override: Partial<Entity> = {}, connectionName?: string): Promise<Entity> {
    const instance = new entity({ ...(await generator(saveInDb, override)), ...override });
    if (saveInDb) {
      await getManager(connectionName).save(entity, instance as any); // as any https://github.com/Microsoft/TypeScript/issues/21592
    }
    return instance;
  }

  /**
   * Creates multiple instances of an entity
   *
   * @see devdocs test-fixtures
   * @param {boolean} saveInDb - whether or not save the created entities in db
   * @param {number} count - the number of entities to create
   * @param {(count?: number) => Partial<Entity>} [getOverride] a function allowing to customize the created objects
   * @returns {Promise<Entity[]>} Resolves to an array containing the entities created
   */
  async function createMany(
    saveInDb: boolean,
    count: number,
    getOverride?: (count: number) => Partial<Entity>,
    connectionName?: string,
  ): Promise<Entity[]>;
  async function createMany(
    saveInDb: boolean,
    count: number,
    getOverride?: (count: number) => Promise<Partial<Entity>>,
    connectionName?: string,
  ): Promise<Entity[]>;
  async function createMany(
    saveInDb: boolean,
    count: number,
    getOverride: (count: number) => Partial<Entity> | Promise<Partial<Entity>> = () => ({}),
    connectionName?: string,
  ): Promise<Entity[]> {
    const results: Entity[] = [];

    for (let i = 0; i < count; i++) {
      results.push(await createOne(saveInDb, await getOverride(i), connectionName));
    }

    return results;
  }

  return { createOne, createMany };
}
