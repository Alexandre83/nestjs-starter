import { Test, TestingModule } from '@nestjs/testing';

import { HooksService } from '../../provider/hooks/hooks.service';
import { IDatabaseService } from '../database-service/database-service';
import { CRUDController, ICRUDController } from './crud.controller';

// tslint:disable:max-classes-per-file
class Fake {
  fakeId: number;

  constructor(copy: Partial<Fake> = {}) {
    this.fakeId = copy.fakeId || (undefined as any);
  }
}

class FakeService {}

class FakePostInDto {}

class FakePutInDto {}

class FakeOutDto {}

class FakeController extends CRUDController(FakeService as any, FakePostInDto, FakePutInDto, FakeOutDto, {
  entityClass: Fake,
  identifierKey: 'fakeId',
  relations: { fakeId: true },
}) {}

describe('CRUDController', () => {
  let module: TestingModule;
  let service: IDatabaseService<Fake>;
  let hooksService: HooksService;
  let controller: ICRUDController<FakeService, FakePostInDto, FakePutInDto, Fake>;

  beforeEach(async () => {
    service = {} as any;
    hooksService = {
      runHooks: jest.fn(),
    } as any;
    module = await Test.createTestingModule({
      providers: [FakeController, { provide: FakeService, useValue: service }, { provide: HooksService, useValue: hooksService }],
    }).compile();

    controller = module.get(FakeController);
  });

  it('should have the injected FakeService<Fake> and methods from CRUDController<Fake>()', () => {
    expect((controller as any).service).toBe(service);
    expect(controller.create).toBeDefined();
    expect(controller.find).toBeDefined();
    expect(controller.findById).toBeDefined();
    expect(controller.update).toBeDefined();
    expect(controller.delete).toBeDefined();
  });

  describe('create', () => {
    let dto: FakePostInDto;
    let mockInserted: Fake;

    beforeEach(() => {
      dto = { fakeId: 123 };
      mockInserted = { ...dto, created: new Date() } as any;
      service.save = jest.fn().mockResolvedValue(mockInserted);
      service.findOneByIdOrFail = jest.fn().mockResolvedValue(mockInserted);
    });

    it('should save the data passed and validated in params', async () => {
      await controller.create(dto);

      expect(service.save).toHaveBeenCalledWith(new Fake(dto));
    });

    it('should retrieve and return the new entity', async () => {
      const result = await controller.create(dto);

      expect(service.findOneByIdOrFail).toHaveBeenCalledWith(mockInserted.fakeId, { relations: { fakeId: true } });
      expect(result).toBe(mockInserted);
    });
  });

  describe('find', () => {
    let entities: Fake[];

    beforeEach(() => {
      entities = [{ fakeId: 123 }];
      service.find = jest.fn().mockResolvedValue(entities);
    });

    it('should retrieve and return all entity from database', async () => {
      const results = await controller.find();

      expect(service.find).toHaveBeenCalledWith({ relations: { fakeId: true } });
      expect(results).toBe(entities);
    });
  });

  describe('findById', () => {
    let entity: Fake;

    beforeEach(() => {
      entity = { fakeId: 123 };
    });

    it('should return the entity passed in params', async () => {
      const result = controller.findById(entity);

      expect(result).toBe(entity);
    });
  });

  describe('update', () => {
    let id: string;
    let dto: FakePutInDto;
    let entity: Fake;

    beforeEach(() => {
      id = '123';
      dto = { fakeId: 1234 };
      entity = { fakeId: 123 };
      service.update = jest.fn();
      service.findOneById = jest.fn().mockResolvedValue(entity);
    });

    it('should update the entity passed in params', async () => {
      const result = await controller.update(id, dto);

      expect(result).toBe(entity);
      expect(service.update).toHaveBeenCalledWith(id, dto);
      expect(service.findOneById).toHaveBeenCalledWith(id, { relations: { fakeId: true } });
    });
  });

  describe('delete', () => {
    let entity: Fake;

    beforeEach(() => {
      entity = { fakeId: 123 };
      service.remove = jest.fn();
    });

    it('should delete entity passed in params', async () => {
      const result = await controller.delete(entity);

      expect(service.remove).toHaveBeenCalledWith(entity);
      expect(result).toBeUndefined();
    });
  });
});
