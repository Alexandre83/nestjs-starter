import { Body, createParamDecorator, Delete, Get, Inject, mixin, Param, Post, Put } from '@nestjs/common';
import { ApiBadRequestResponse, ApiImplicitBody, ApiImplicitParam, ApiNotFoundResponse, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import _ from 'lodash';
import { HooksService } from '../../provider/hooks/hooks.service';

import { Output } from '../../interceptor/output-class-serializer';
import { TypedValidationPipe } from '../../pipe/typed-validation.pipe';
import { IDatabaseService } from '../database-service/database-service';
import { RelationsMap } from '../entity-query.definition';

// tslint:disable:member-ordering
export interface ICRUDController<T, U, V, E> {
  service: T | IDatabaseService<E>;
  /**
   * Insert new instance of `T` in database
   *
   * @param dto - validated data
   *
   * @returns Resolves with new entity inserted but formatted
   */
  create(dto: U, ...args: any[]): Promise<E>;

  /**
   * Remove entity identified by id.
   *
   * @param item - entity identified by id passed in route
   *
   * @throws NotFoundException - when no entity corresponding
   *
   * @returns nothing
   */
  delete(item: E, ...args: any[]): Promise<void>;

  /**
   * Retrieve all entities of `T` in database.
   *
   * @returns Resolves with the list of entities
   */
  find(...args: any[]): Promise<E[]>;

  /**
   * Retrieve specific entity of `T` in database identified by id.
   *
   * @param item - entity identified by id passed in route
   *
   * @throws NotFoundException - when no entity corresponding
   *
   * @returns Resolves with the found entity.
   */
  findById(item: E, ...args: any[]): Promise<E>;

  /**
   * Update one entity identified by id passed in route.
   *
   * @param id - id passed in route
   * @param dto - validated data
   *
   * @throws NotFoundException - when no entity corresponding
   *
   * @returns Resolves with the updated entity.
   */
  update(id: string, dto: V, ...args: any[]): Promise<E>;
}

/**
 * Expose to configuration:
 * - @param identifierKey - key identifier of entity, in common cases it's primary key name (ex: `userId`)
 * - @param entityClass - Class of entity, to get some infos
 * - @param relations - Relations to populate during retrieving
 */
export interface ICRUDControllerOptions<T> {
  entityClass: new (...args: any[]) => T;
  identifierKey: keyof T;
  relations?: RelationsMap<T>;
}

/**
 * Create a controller to handle common CRUD operations on `T`
 * It will define:
 *   - 1 GET on `/` of controller to find all occurences of `T` entity
 *   - 2 GET on `/:${identifierKey}` of controller to find one occurence of `T` entity identified by identifierKey value
 *   - 3 POST on `/` of controller to insert new instance of `T` with data passed in body with specific type `U`
 *   - 4 PUT on `/:${identifierKey}` of controller to update instance of `T` with data passed in body with specific type `V`
 *   - 5 DELETE on `/:${identifierKey}` of controller to update instance of `T` with data passed in body with specific type `V`
 *
 * All this routes serialize output data with dto `W`.
 *
 * ⚠️ Requires a middleware that automatically populate id passed in route ⚠️
 *
 * @param serviceClass - Class of service that will be used
 * @param postInDto - Dto to create an entity `T`
 * @param putInDto - Dto to update an entity `T`
 * @param outDto - Dto to serialize output data
 *
 * @returns CRUDController instance that can be extended with some others methods
 */
export function CRUDController<T, U, V, W, E>(
  serviceClass: T | (new () => IDatabaseService<E>),
  postInDto: new () => U,
  putInDto: new () => V,
  outDto: new () => W,
  options: ICRUDControllerOptions<E>,
): new (...args: any[]) => ICRUDController<T, U, V, E> {
  const { entityClass, identifierKey, relations } = options;
  const EntityParam = createParamDecorator((data, req) => req.api[_.camelCase(entityClass.name)]);

  class CommonCRUDController {
    @Inject(serviceClass) protected service: IDatabaseService<E>;
    @Inject(HooksService) private readonly hookService: HooksService;

    @Post()
    @ApiOperation({ title: `Add one ${entityClass.name}`, description: `${entityClass.name} will be inserted with data passed in body.` })
    @ApiImplicitBody({ name: postInDto.name, type: postInDto })
    @ApiOkResponse({ description: `${entityClass.name} has been created.`, type: outDto })
    @ApiBadRequestResponse({ description: `Bad property used to create one ${entityClass.name}.` })
    @Output(outDto)
    async create(@Body(new TypedValidationPipe(postInDto)) dto: U): Promise<E> {
      const { [identifierKey]: id } = await this.service.save(new entityClass(dto));

      return this.service.findOneByIdOrFail(id as any, { relations });
    }

    @Delete(`:${identifierKey}`)
    @ApiOperation({
      title: `Delete one specific ${entityClass.name}`,
      description: `${entityClass.name} will be deleted and identified with id passed in route.`,
    })
    @ApiImplicitParam({ name: identifierKey as string, type: 'string', required: true, description: `${entityClass.name} identifier` })
    @ApiOkResponse({ description: `${entityClass.name} has been deleted.` })
    @ApiNotFoundResponse({ description: `${entityClass.name} was not found with its identifier.` })
    async delete(@EntityParam() item: E) {
      await this.service.remove(item);
    }

    @Get()
    @ApiOperation({
      title: `Get all ${entityClass.name}`,
      description: `Return the list of all ${entityClass.name} from database.`,
    })
    @ApiOkResponse({ description: `All ${entityClass.name}s found.`, type: outDto })
    @Output(outDto)
    find() {
      return this.service.find({ relations });
    }

    @Get(`:${identifierKey}`)
    @ApiOperation({
      title: `Get one specific ${entityClass.name}`,
      description: `${entityClass.name} will be returned and identified with id passed in route.`,
    })
    @ApiImplicitParam({ name: identifierKey as string, type: 'string', required: true, description: `${entityClass.name} identifier` })
    @ApiOkResponse({ description: `${entityClass.name} returned.`, type: outDto })
    @ApiNotFoundResponse({ description: `${entityClass.name} was not found with its identifier.` })
    @Output(outDto)
    findById(@EntityParam() item: E) {
      return item;
    }

    @Put(`:${identifierKey}`)
    @ApiOperation({
      title: `Update one specific ${entityClass.name}`,
      description: `${entityClass.name} will be updated and identified with id passed in route and with data passed in body.`,
    })
    @ApiImplicitBody({ name: putInDto.name, type: putInDto })
    @ApiImplicitParam({ name: identifierKey as string, type: 'string', required: true, description: `${entityClass.name} identifier` })
    @ApiOkResponse({ description: `${entityClass.name} has been deleted.`, type: outDto })
    @ApiNotFoundResponse({ description: `${entityClass.name} was not found with its identifier.` })
    @Output(outDto)
    async update(@Param(identifierKey as string) id: string, @Body(new TypedValidationPipe(putInDto)) dto: V, @EntityParam() item: E) {
      this.hookService.runHooks('beforeUpdate', _.startCase(entityClass.name) + 'Service', item);
      await this.service.update(id, dto);
      const updated = await this.service.findOneById(id, { relations });
      this.hookService.runHooks('afterUpdate', _.startCase(entityClass.name) + 'Service', updated);
      return updated;
    }
  }

  return mixin(CommonCRUDController);
}
