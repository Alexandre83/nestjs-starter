import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import request from 'supertest';
import { getManager } from 'typeorm';

import { AuthModule } from '../../../user/module/auth/auth.module';
import { DatabaseModule } from '../../database-config/database.module';
import { UserTypeEnum } from '../../enum/user-type.enum';
import { generateAuthenticatedUserHeaders } from '../authenticated-user/authenticated-user.e2e.tools';
import { IDatabaseService } from '../database-service/database-service';
import { RelationsMap } from '../entity-query.definition';
import { omit, pick } from '../utility.tools';

export interface IProtectedOptions {
  type: UserTypeEnum;
}

export interface ISubEntities<T> {
  class: new () => any;
  key: keyof T;
}

export interface IWithoutSpecificTests {
  all: {
    get?: boolean;
    post?: boolean;
  };
  one: {
    delete?: boolean;
    get?: boolean;
    put?: boolean;
  };
}

export function runE2ECRUD<T, U, V, W, E>(
  baseUrl: string,
  entitiesFn: (
    saveInDb: boolean,
    count: number,
    getOverride?: ((count: number) => Promise<Partial<T>>) | undefined,
    connectionName?: string,
  ) => Promise<T[]>,
  moduleToInject: any,
  serviceToInject: E | (new () => IDatabaseService<T>),
  dtoPost: (entities: T[]) => Promise<U> | U,
  dtoPut: (entities: T[]) => Promise<V> | V,
  primaryKey: keyof T,
  formatFn: (entity: T, id?: any) => W,
  entityClass: new (...args: any[]) => T,
  relations?: RelationsMap<T>,
  protectedOptions?: IProtectedOptions,
  subEntities?: Array<ISubEntities<T>>,
  withoutSpecificTests: IWithoutSpecificTests = { all: {}, one: {} },
  connectionName?: string,
) {
  let app: INestApplication;
  let server: any;
  let service: IDatabaseService<T>;
  let patternUrl: string;
  let headers: any;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [DatabaseModule, AuthModule, moduleToInject],
    }).compile();

    app = module.createNestApplication();
    service = module.get(serviceToInject as any);
    await app.init();
    server = app.getHttpServer();
  });

  afterAll(() => {
    app.close();
  });

  let entities: T[];

  beforeEach(async () => {
    patternUrl = baseUrl;
    entities = await entitiesFn(true, 3, undefined, connectionName);
    headers = {};

    if (protectedOptions && protectedOptions.type) {
      try {
        const { headers: authenticatedHeaders } = await generateAuthenticatedUserHeaders(protectedOptions.type);
        headers = authenticatedHeaders;
      } catch (e) {
        throw e;
      }
    }
  });

  if (withoutSpecificTests.all.post !== true) {
    describe('POST', () => {
      it(`should insert new ${entityClass.name.toLowerCase()} in database and return it`, async () => {
        const data: T = (await dtoPost(entities)) as any;

        if (subEntities && subEntities.length) {
          const promises = subEntities.map(item => {
            return getManager(connectionName).save(item.class, data[item.key]) as Promise<any>;
          });

          await Promise.all(promises);
        }

        const { body } = await request(server)
          .post(patternUrl)
          .set(headers)
          .send(data as any)
          .expect(HttpStatus.CREATED);

        const inserted = await service.findOneById(body[primaryKey], { relations });

        expect(body).toEqual(formatFn(inserted!));
      });
    });
  }

  if (withoutSpecificTests.all.get !== true) {
    describe('GET', () => {
      it(`should return the list of all ${entityClass.name.toLowerCase()} from database`, async () => {
        const { body } = await request(server)
          .get(patternUrl)
          .set(headers)
          .expect(HttpStatus.OK);

        const entitiesFromDb = await service.find({ relations });
        expect(body).toEqual(entitiesFromDb.map(entity => formatFn(entity)));
      });
    });
  }

  if (!(withoutSpecificTests.one.get === true && withoutSpecificTests.one.put === true && withoutSpecificTests.one.delete === true)) {
    describe(`/:${primaryKey}`, () => {
      let id: string;
      let entity: T;

      beforeEach(() => {
        entity = entities[0];
        id = String(entity[primaryKey]);
        patternUrl = `${patternUrl}/${id}`;
      });

      if (withoutSpecificTests.one.get !== true) {
        describe('GET', () => {
          it(`should return the ${entityClass.name.toLowerCase()} identified by its id passed in routes`, async () => {
            const { body } = await request(server)
              .get(patternUrl)
              .set(headers)
              .expect(HttpStatus.OK);

            const entityFromDb = await service.findOneById(id, { relations });
            expect(body).toEqual(formatFn(entityFromDb!));
          });
        });
      }

      if (withoutSpecificTests.one.put !== true) {
        describe('PUT', () => {
          it(`should update and return the ${entityClass.name.toLowerCase()} identified by its id passed in routes`, async () => {
            const dto = await dtoPut(entities);

            const { body } = await request(server)
              .put(patternUrl)
              .set(headers)
              .send(dto as any)
              .expect(HttpStatus.OK);

            const updated = await service.findOneById(id, { relations });

            expect(body).toEqual(formatFn(updated!, +id));
            expect(dto).toMatchObject({ ...pick(omit(formatFn(updated!) as any, primaryKey), ...(Object.keys(dto) as any)) });
          });
        });
      }

      if (withoutSpecificTests.one.delete !== true) {
        describe('DELETE', () => {
          it(`should remove from database the ${entityClass.name.toLowerCase()} identified by its id passed in the route`, async () => {
            await request(server)
              .delete(patternUrl)
              .set(headers)
              .expect(HttpStatus.OK);

            const removed = await service.findOneById(id, { relations });

            expect(removed).toBeUndefined();
          });
        });
      }
    });
  }
}
