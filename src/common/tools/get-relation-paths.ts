import { flatten } from 'flat';

import { RelationsMap } from './entity-query.definition';

/**
 * Convert the given RelationMap to an array of paths to relations.
 * Helps populating entities being requested by TypeORM.
 *
 * @param relationsMap Object defining (with ̀true`) the relations to
 * be loaded.
 *
 * @return Array of paths to relations to be loaded by TypeORM.
 *
 * @example
 *
 * ```
 * getRelationsPaths({
 *   ape: true,
 *   address: true,
 * })
 * // returns: ['ape', 'address']
 * ```
 */
export function getRelationsPaths<Entity>(relationsMap: RelationsMap<Entity> | undefined): string[] | undefined {
  if (!relationsMap) {
    return undefined;
  }

  const flattenObject = flatten(relationsMap);
  const paths = new Set<string>();

  return Array.from(
    Object.entries(flattenObject).reduce((acc, [key, value]) => {
      if (value === false) {
        // no population => no new path
        return acc;
      }

      const segments = key.split('.');
      const pathsGroup = [segments[0]];

      if (segments.length > 1) {
        for (let i = 2; i < segments.length + 1; ++i) {
          pathsGroup.push(segments.slice(0, i).join('.'));
        }
      }

      for (const pathGroup of pathsGroup) {
        acc.add(pathGroup);
      }
      return acc;
    }, paths),
  ).filter(key => key);
}
