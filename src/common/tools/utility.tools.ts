import _ from 'lodash';

export function pick<T, K extends keyof T>(obj: T, ...keys: K[]): Pick<T, K> {
  return _.pick(obj, keys) as any;
}

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;
export function omit<T extends object, K extends keyof T>(obj: T, ...keys: K[]): Omit<T, K> {
  return _.omit(obj, keys) as any;
}
