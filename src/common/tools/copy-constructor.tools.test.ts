import { getCopyConstruction, getCopyConstructions, getOrDefault } from './copy-constructor.tools';

class Test {
  firstName: string;
  lastName: string;
  testId: string;

  constructor(copy: Partial<Test> = {}) {
    this.testId = getOrDefault(copy.testId, '');
    this.lastName = getOrDefault(copy.lastName, '');
    this.firstName = getOrDefault(copy.firstName, '');
  }
}

describe('misc', () => {
  describe('getCopyConstruction', () => {
    it('returns undefined if source is undefined', () => {
      const result = getCopyConstruction(Test, undefined);
      expect(result).toBe(undefined);
    });

    it('returns undefined if source is null', () => {
      const result = getCopyConstruction(Test, null!);
      expect(result).toBe(undefined);
    });

    it('returns a new instance built with copy constructor', () => {
      const source = new Test({
        testId: '57785382-a93c-4ba3-8f6d-5ada03a6c98c',
        firstName: 'Brian',
      });
      const result = getCopyConstruction(Test, source);
      expect(result).toEqual(source);
      expect(result).not.toBe(source);
    });
  });

  describe('getOrDefault', () => {
    it('returns undefined if source and default are undefined', () => {
      const result = getOrDefault(undefined, undefined);
      expect(result).toBe(undefined);
    });

    it('returns default value (primitive) if source is undefined', () => {
      const defaultValue = 5;
      const result = getOrDefault(undefined, defaultValue);
      expect(result).toBe(defaultValue);
    });

    it('returns default value (object) if source is undefined', () => {
      const defaultValue = new Test();
      const result = getOrDefault(undefined, defaultValue);
      expect(result).toBe(defaultValue);
    });

    it('returns the source if not undefined', () => {
      const source = 'john';
      const defaultValue = 'mike';
      expect(getOrDefault(source as any, defaultValue)).toBe(source);
    });
  });

  describe('getCopyConstructions', () => {
    it('returns undefined if sources is undefined', () => {
      const result = getCopyConstructions(Test, undefined);
      expect(result).toBe(undefined);
    });

    it('returns undefined if sources is null', () => {
      const result = getCopyConstructions(Test, null!);
      expect(result).toBe(undefined);
    });

    it('returns an array of new instances built with copy constructors', () => {
      const sources = [
        new Test({
          testId: '57785382-a93c-4ba3-8f6d-5ada03a6c98c',
          firstName: 'Jack',
          lastName: 'Elaygarson',
        }),
        new Test({
          testId: 'fd3befb9-fdd7-4239-877b-0655e2c5769c',
          firstName: 'Helen',
          lastName: 'Daniels',
        }),
      ];

      const results = getCopyConstructions(Test, sources);
      expect(results).toEqual(sources);
      expect(results).not.toBe(sources);
    });

    it('strips undefined items from the source array before returning the copies', () => {
      const sources = [
        new Test({
          testId: '57785382-a93c-4ba3-8f6d-5ada03a6c98c',
          firstName: 'Jack',
          lastName: 'Elaygarson',
        }),
        undefined,
        new Test({
          testId: 'fd3befb9-fdd7-4239-877b-0655e2c5769c',
          firstName: 'Helen',
          lastName: 'Daniels',
        }),
        undefined,
      ];

      const results = getCopyConstructions(Test, sources as Test[]);

      const sourcesWithoutUndefined = sources.filter(test => test);
      expect(results).toEqual(sourcesWithoutUndefined);
    });
  });
});
