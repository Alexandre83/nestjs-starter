import { FindOperator } from 'typeorm';

type ValueType = number | string | boolean | Date;
type NonEntity = ValueType | null | undefined | never | ((...args: any[]) => any);

/** Properties of T matching with `NonEntity | NonEntity[]`. */
export type NonEntityProps<T> = { [P in keyof T]: T[P] extends NonEntity | NonEntity[] ? P : never }[keyof T];
/** Properties of T not matching with `NonEntity | NonEntity[]`. */
export type EntityProps<T> = { [P in keyof T]: T[P] extends NonEntity | NonEntity[] ? never : P }[keyof T];
/** T usable as a "where" condition: only keeps `NonEntity | NonEntity[]` properties. */
export type WhereClause<T> = { [P in NonEntityProps<T>]?: T[P] | FindOperator<T[P]> };
/** Relations map of T: allow building a "population" map. */
export type RelationsMap<T> = { [P in EntityProps<T>]?: (T[P] extends Array<infer U> ? RelationsMap<U> : RelationsMap<T[P]>) | boolean };

export interface IFindOneOptions<T> {
  /** Order in which entities should be returned */
  order?: { [P in NonEntityProps<T>]?: 'ASC' | 'DESC' | 1 | -1 };
  /** Relations which should be loaded ("left join" shortcut) */
  relations?: RelationsMap<T>;
  /** Columns which should be retrieved */
  select?: Array<NonEntityProps<T>>;
  /** Where clause used to match entities */
  where?: WhereClause<T> | Array<WhereClause<T>>;
}

export interface IFindManyOptions<T> extends IFindOneOptions<T> {
  /** Offset from where entities should be taken in result */
  skip?: number;
  /** Limit of entities (max number) which should be taken */
  take?: number;
}
