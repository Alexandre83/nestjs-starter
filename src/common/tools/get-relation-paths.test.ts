import { RelationsMap } from './entity-query.definition';
import { getRelationsPaths } from './get-relation-paths';

class FakeDeepSubEntity {
  id: string;
  name: string;
}

// tslint:disable-next-line:max-classes-per-file
class FakeSubEntity {
  deepEntity: FakeDeepSubEntity;
  id: string;
  name: string;
  parent: FakeSubEntity;
}

// tslint:disable-next-line:max-classes-per-file
class FakeEntity {
  id: string;
  secondSubEntity: FakeSubEntity;
  subEntity: FakeSubEntity;
}

describe('getRelationsPath', () => {
  it('returns all paths leading to a population (`true`)', () => {
    const relationsMap: RelationsMap<FakeEntity> = {
      subEntity: false,
      secondSubEntity: true,
    };

    const paths = getRelationsPaths(relationsMap);
    expect(paths).toEqual(['secondSubEntity']);
  });

  it('handles nested populations', () => {
    const relationsMap: RelationsMap<FakeEntity> = {
      subEntity: true,
      secondSubEntity: { deepEntity: true },
    };

    const paths = getRelationsPaths(relationsMap);
    // expect(paths.length).toBe(9);
    expect(paths).toEqual(['subEntity', 'secondSubEntity', 'secondSubEntity.deepEntity']);
  });

  it('handles multiple nested populations', () => {
    const relationsMap: RelationsMap<FakeEntity> = {
      subEntity: true,
      secondSubEntity: { deepEntity: true, parent: { deepEntity: true } },
    };

    const paths = getRelationsPaths(relationsMap);
    expect(paths).toEqual([
      'subEntity',
      'secondSubEntity',
      'secondSubEntity.deepEntity',
      'secondSubEntity.parent',
      'secondSubEntity.parent.deepEntity',
    ]);
  });
});
