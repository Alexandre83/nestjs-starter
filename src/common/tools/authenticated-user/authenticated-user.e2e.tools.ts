import { createUserInformation } from '../../../user/module/user-information/user-information.test.tools';
import { createUserType } from '../../../user/module/user-type/user-type.test.tools';
import { UserTypeEnum } from '../../enum/user-type.enum';

/**
 * Save a new userInformation with new user and token couple.
 *
 * Return headers to simulate a authenticated user.
 */
export async function generateAuthenticatedUserHeaders(type: UserTypeEnum) {
  const userType = await createUserType(true, { name: type });
  const userInformation = await createUserInformation(true, { userType });

  return { headers: { Authorization: `bearer ${userInformation.token!.accessToken}` }, userInformation };
}
