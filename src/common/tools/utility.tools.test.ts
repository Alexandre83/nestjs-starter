import _ from 'lodash';

import { omit, pick } from './utility.tools';

describe('pick', () => {
  it('calls lodash pick', () => {
    const spy = jest.spyOn(_, 'pick');
    const obj = { necessary: 'necessary', unnecessary: 'unnecessary' };
    const key = 'necessary';

    const result = pick(obj, key);
    expect(result).toEqual({ necessary: 'necessary' });
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toBeCalledWith(obj, [key]);
  });
});

describe('omit', () => {
  it('calls lodash omit', () => {
    const spy = jest.spyOn(_, 'omit');
    const obj = { necessary: 'necessary', unnecessary: 'unnecessary' };
    const key = 'unnecessary';

    const result = omit(obj, key);
    expect(result).toEqual({ necessary: 'necessary' });
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toBeCalledWith(obj, [key]);
  });
});
