import { getConnection } from 'typeorm';

/**
 * Constraint data return from `information_schema.table_constraints`
 */
export interface IConstraintDatabase {
  constraint_name: string;
  table_name: string;
}

/**
 * Returns all constraints database list
 *
 * N.B: Like a singleton pattern, the table list will be get from DB only once
 *      The `constraintsList` global variable is used
 *
 * @param dbName - database name
 * @param schema - schema name
 * @param {connectionName} - truncate table of specific connection
 * @returns {Promise<IConstraintDatabase[]>}
 */
async function getConstraints(dbName: string, schema: string, connectionName?: string): Promise<IConstraintDatabase[]> {
  const connection = getConnection(connectionName);
  const constraintsList = await connection.query(
    `SELECT table_name, constraint_name FROM information_schema.table_constraints WHERE constraint_schema='${schema}' AND constraint_type = 'FOREIGN KEY' AND table_catalog='${dbName}'`,
  );

  return constraintsList;
}

/**
 * Returns database table list
 *
 * N.B: Like a singleton pattern, the table list will be get from DB only once
 *      The `tableNames` global variable is used
 *
 * @param dbName - database name
 * @param schema - schema name
 * @param {connectionName} - truncate table of specific connection
 * @returns {Promise<string[]>}
 */
async function getTables(dbName: string, schema: string, connectionName?: string): Promise<string[]> {
  const connection = getConnection(connectionName);
  const items: any[] = await connection.query(
    `SELECT table_name FROM information_schema.tables WHERE table_schema='${schema}' AND table_type='BASE TABLE' AND table_catalog='${dbName}'`,
  );
  const tableNames = items.map(item => (item as any).table_name);

  return tableNames;
}

/**
 * Flush all databases
 *
 * @returns {Promise<void>}
 */
export async function truncateTables(): Promise<void> {
  await truncateTablesOfOneDatabase(process.env.USERDB_NAME!, undefined, process.env.USERDB_SCHEMA);
  await truncateTablesOfOneDatabase(process.env.DATADB_NAME!, process.env.DATA_NAME, process.env.DATADB_SCHEMA);
  await truncateTablesOfOneDatabase(process.env.DEVICEDB_NAME!, process.env.DEVICE_NAME, process.env.DEVICEDB_SCHEMA);
}

/**
 * Flush all database tables
 *
 * @param dbName - database name
 * @param connectionName - truncate table of specific connection
 * @param schema - schema name, default public
 * @returns {Promise<void>}
 */
export async function truncateTablesOfOneDatabase(dbName: string, connectionName?: string, schema: string = 'public') {
  try {
    const connection = getConnection(connectionName);

    const tables = await getTables(dbName, schema, connectionName);
    const constraints = await getConstraints(dbName, schema, connectionName);
    await connection.transaction(async transactionalEntityManager => {
      // Put all constraints deferrable
      await Promise.all(
        constraints.map(constraint =>
          transactionalEntityManager.query(
            `ALTER TABLE "${schema}"."${constraint.table_name}" ALTER CONSTRAINT "${constraint.constraint_name}" DEFERRABLE`,
          ),
        ),
      );
      await transactionalEntityManager.query('SET CONSTRAINTS ALL DEFERRED');
      // Remove all data within database
      await Promise.all(tables.map(table => transactionalEntityManager.query(`DELETE FROM "${schema}"."${table}"`)));
    });
    // revert all constraints deferrable
    await Promise.all(
      constraints.map(constraint =>
        connection.query(`ALTER TABLE "${schema}"."${constraint.table_name}" ALTER CONSTRAINT "${constraint.constraint_name}" NOT DEFERRABLE`),
      ),
    );
  } catch (e) {
    if (/Connection ".*" was not found\./.test(e.message)) {
      return;
    }
    throw e;
  }
}
