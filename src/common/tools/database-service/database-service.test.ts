import { NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { NextFunction, Response } from 'express';
import * as faker from 'faker';
import { Repository } from 'typeorm';
import { ColumnMetadata } from 'typeorm/metadata/ColumnMetadata';

import { IApiRequest } from '../../interface/api-request.interface';
import { IFindManyOptions, IFindOneOptions, RelationsMap, WhereClause } from '../entity-query.definition';
import { DatabaseService } from './database-service';

describe('DatabaseService', () => {
  class FakeEntity {
    date: Date;
    id: string;
    name: string;
    value: number;
  }

  // tslint:disable-next-line:max-classes-per-file
  class FakeEntityService extends DatabaseService<FakeEntity>(FakeEntity) {}

  let fakeService: FakeEntityService;
  let fakeServiceAsAny: any;
  let repositoryMock: Repository<FakeEntity>;
  let findOptions: IFindOneOptions<FakeEntity> | IFindManyOptions<FakeEntity>;
  let id: string;

  beforeEach(async () => {
    repositoryMock = {
      count: jest.fn(),
      delete: jest.fn(),
      find: jest.fn(),
      findByIds: jest.fn(),
      findOne: jest.fn(),
      findOneOrFail: jest.fn(),
      findOneById: jest.fn(),
      isExisting: jest.fn(),
      update: jest.fn(),
    } as any;

    const module = await Test.createTestingModule({
      providers: [FakeEntityService, { provide: getRepositoryToken(FakeEntity), useValue: repositoryMock }],
    }).compile();

    fakeService = module.get(FakeEntityService);
    fakeServiceAsAny = fakeService as any;

    id = faker.random.uuid();
    findOptions = { where: { id } };
  });

  describe('count', () => {
    it('calls repository.count with the given options and returns its value', async () => {
      const resultFromRepo = {};
      repositoryMock.count = jest.fn().mockResolvedValue(resultFromRepo);
      const result = await fakeService.count(findOptions);
      expect(repositoryMock.count).toHaveBeenCalledWith(findOptions);
      expect(result).toBe(resultFromRepo);
    });
  });

  describe('buildEntityFromRawResult', () => {
    let ownColumns: ColumnMetadata[];
    let alias: string;
    let rawItem: { [k: string]: string };

    beforeEach(() => {
      alias = 'ac';
      ownColumns = [];
      ownColumns.push(
        {
          propertyName: 'accountId',
          databaseName: 'account_id',
        } as any,
        {
          propertyName: 'name',
          databaseName: 'name',
        } as any,
        {
          propertyName: 'jobTitleId',
          databaseName: 'job_title_id',
        } as any,
      );

      rawItem = ownColumns.reduce(
        (acc, val) => {
          acc[`${alias}_${val.databaseName}`] = `fake_${val.propertyName}`;
          return acc;
        },
        {} as typeof rawItem,
      );

      (repositoryMock as any).metadata = { ownColumns };
    });

    it('builds an object from the given raw item', () => {
      const result = fakeService.buildEntityFromRawResult(rawItem, alias);
      expect(result).toEqual({
        accountId: 'fake_accountId',
        jobTitleId: 'fake_jobTitleId',
        name: 'fake_name',
      });
    });

    it('omits undefined properties in the result', () => {
      delete rawItem[`${alias}_account_id`];

      const result = fakeService.buildEntityFromRawResult(rawItem, alias);
      expect(result).toEqual({
        // accountId is not present in the output ✔
        jobTitleId: 'fake_jobTitleId',
        name: 'fake_name',
      });
    });
  });

  describe('getEntityQueryMapping', () => {
    beforeEach(
      () =>
        ((repositoryMock as any).metadata = {
          name: 'AnyClass',
          ownColumns: [{ propertyName: 'userId', databaseName: 'user_id' }, { propertyName: 'name', databaseName: 'name' }],
        }),
    );

    it('returns the mapping using class name prefix', () => {
      const result = fakeService.getEntityQueryMapping();
      expect(result).toEqual({
        name: 'AnyClass_name',
        userId: 'AnyClass_user_id',
      });
    });

    it('returns the mapping using alias prefix', () => {
      const result = fakeService.getEntityQueryMapping('alias');
      expect(result).toEqual({
        name: 'alias_name',
        userId: 'alias_user_id',
      });
    });
  });

  describe('findOne', () => {
    it('calls repository.findOne with the given options and returns its result', async () => {
      const resultFromRepo = {};
      repositoryMock.findOne = jest.fn().mockResolvedValue(resultFromRepo);
      const result = await fakeService.findOne(findOptions);
      expect(repositoryMock.findOne).toHaveBeenCalledWith(findOptions);
      expect(resultFromRepo).toBe(result);
    });
  });

  describe('findOneById', () => {
    it('calls repository.findOneById with the given id and returns its value', async () => {
      const resultFromRepo = {};
      repositoryMock.findOne = jest.fn().mockResolvedValue(resultFromRepo);
      const result = await fakeService.findOneById(id);
      expect(repositoryMock.findOne).toHaveBeenCalledWith(id, undefined);
      expect(resultFromRepo).toBe(result);
    });

    it('calls repository.findOneById with the given id and options', async () => {
      const resultFromRepo = {};
      repositoryMock.findOne = jest.fn().mockResolvedValue(resultFromRepo);

      const result = await fakeService.findOneById(id, findOptions);
      expect(repositoryMock.findOne).toHaveBeenCalledWith(id, findOptions);
      expect(result).toBe(resultFromRepo);
    });
  });

  describe('findByIds', () => {
    it('calls repository.findByIds with the given id', async () => {
      const resultFromRepo = [{}];
      repositoryMock.findByIds = jest.fn().mockResolvedValue(resultFromRepo);

      const result = await fakeService.findByIds([id]);
      expect(repositoryMock.findByIds).toHaveBeenCalledWith([id], undefined);
      expect(result).toBe(resultFromRepo);
    });

    it('calls repository.findByIds with the given id and options', async () => {
      await fakeService.findByIds([id], findOptions);
      expect(repositoryMock.findByIds).toHaveBeenCalledWith([id], findOptions);
    });
  });

  describe('find', () => {
    it('calls repository.find with the given options', async () => {
      const resultFromRepo = {};
      repositoryMock.find = jest.fn().mockResolvedValue(resultFromRepo);

      const result = await fakeService.find(findOptions);
      expect(repositoryMock.find).toHaveBeenCalledWith(findOptions);
      expect(resultFromRepo).toBe(result);
    });
  });

  describe('isExisting', () => {
    it('calls repository.count with the given where clause', async () => {
      const where = { name: 'Gojob' };
      repositoryMock.count = jest.fn().mockResolvedValue(2);

      const result = await fakeService.isExisting(where);
      expect(repositoryMock.count).toHaveBeenCalledWith({ where });
      expect(result).toBe(true);
    });

    it('resolves with false when the count is 0', async () => {
      const where = { name: 'Gojob' };
      repositoryMock.count = jest.fn().mockResolvedValue(0);

      const result = await fakeService.isExisting(where);
      expect(result).toBe(false);
    });
  });

  describe('insert', () => {
    it('inserts the given (partial) entity and returns the result of the operation', async () => {
      const expectedResult = {};
      repositoryMock.insert = jest.fn().mockResolvedValue(expectedResult);

      const partialEntity = {};
      const result = await fakeService.insert(partialEntity);
      expect(result).toBe(expectedResult);
      expect(repositoryMock.insert).toHaveBeenCalledWith(partialEntity);
    });
  });

  describe('update', () => {
    it('calls repository.update with the given clause and update', async () => {
      const where: WhereClause<FakeEntity> = {
        id: faker.random.uuid(),
        name: faker.random.word(),
      };

      const update: Partial<FakeEntity> = {
        name: faker.random.word(),
      };

      await fakeService.update(where, update);
      expect(repositoryMock.update).toHaveBeenCalledWith(where, update);
    });
  });

  describe('delete', () => {
    it('calls repository.delete with the given clause', async () => {
      const resultFromRepo = {};
      repositoryMock.delete = jest.fn().mockResolvedValue(resultFromRepo);

      const where: WhereClause<FakeEntity> = {
        id: faker.random.uuid(),
        name: faker.random.word(),
      };

      const result = await fakeService.delete(where);
      expect(repositoryMock.delete).toHaveBeenCalledWith(where);
      expect(result).toBe(resultFromRepo);
    });
  });

  describe('save', () => {
    it('calls repository.save with the given entity(ies)', async () => {
      const entities = [{}, {}];
      const options = { name: 'toto' };
      repositoryMock.save = jest.fn().mockResolvedValue(entities);

      const result = await fakeService.save(entities as any, options as any);
      expect(repositoryMock.save).toHaveBeenCalledWith(entities, options);
      expect(result).toBe(entities);
    });
  });

  describe('createFindByMiddleware', () => {
    /* Most of the testing cannot be done using unit tests, as
    the metadata of repositories (built by TypeORM) requires
    a connection to a database */

    // tslint:disable-next-line:ban-types
    let middleware: Function;
    let parameterName: string;
    let reqPropertyName: string;
    let spyGetPrimaryIdName: jest.SpyInstance;
    let req: IApiRequest;
    let res: Response;
    let next: NextFunction;

    beforeEach(() => {
      middleware = fakeService.createFindByMiddleware();
      parameterName = 'companyId';
      reqPropertyName = 'company';

      spyGetPrimaryIdName = jest.spyOn(fakeService, 'getPrimaryIdNameOrThrow' as any);
      spyGetPrimaryIdName.mockReturnValue('companyId');

      req = { api: {}, params: {} } as any;
      res = {} as any;
      next = jest.fn();
    });

    it('returns a middleware function', () => {
      expect(typeof middleware).toBe('function');
    });

    it('throws when no parameterName is given and no metadata is available', async () => {
      spyGetPrimaryIdName.mockRestore();

      expect.assertions(1);
      try {
        await middleware();
      } catch (e) {
        expect(e).toBeInstanceOf(TypeError);
      }
    });

    it('calls given next() if parameter is not found in the request config', async () => {
      middleware = fakeService.createFindByMiddleware({ parameterName });
      await middleware(req, res, next);

      expect(next).toHaveBeenCalled();
    });

    it('throws when parameterName is given but no reqPropertyName nor metadata are available', async () => {
      middleware = fakeService.createFindByMiddleware({ parameterName });
      req.params.companyId = faker.random.uuid();
      jest.spyOn(fakeService, 'findOne').mockResolvedValue({} as any);

      expect.assertions(1);
      try {
        await middleware(req, res, next);
      } catch (e) {
        expect(e).toBeInstanceOf(TypeError);
      }
    });

    it('calls findOne when parameterName and reqPropertyName are valid', async () => {
      middleware = fakeService.createFindByMiddleware({
        parameterName,
        reqPropertyName,
        throwIfNotFound: false,
      });
      req.params.companyId = faker.random.uuid();
      jest.spyOn(fakeService, 'findOne');

      await middleware(req, res, next);
      expect(fakeService.findOne).toHaveBeenCalled();
      expect(repositoryMock.findOne).toHaveBeenCalled();
    });

    it('calls findOne when queryField is given', async () => {
      const customParamName = 'michel';
      middleware = fakeService.createFindByMiddleware({
        reqPropertyName,
        parameterName: customParamName,
        throwIfNotFound: false,
        queryField: 'id',
      });
      req.params[customParamName] = faker.random.uuid();
      jest.spyOn(fakeService, 'findOne');

      await middleware(req, res, next);
      expect(fakeService.findOne).toHaveBeenCalledWith({
        where: { id: (req.params as any)[customParamName] },
      });
      expect(repositoryMock.findOne).toHaveBeenCalled();
    });

    it('throws a NotFoundException when no entity found and throwIfNotFound not disabled', async () => {
      middleware = fakeService.createFindByMiddleware({
        parameterName,
        reqPropertyName,
      });
      req.params.companyId = faker.random.uuid();

      expect.assertions(2);
      try {
        await middleware(req, res, next);
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundException);
        expect(repositoryMock.findOne).toHaveBeenCalled();
      }
    });

    it('throws a NotFoundException if no entity found and checkExistenceOnly enabled', async () => {
      middleware = fakeService.createFindByMiddleware({
        checkExistenceOnly: true,
        parameterName,
        reqPropertyName,
      });
      req.params.companyId = faker.random.uuid();

      expect.assertions(2);
      try {
        await middleware(req, res, next);
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundException);
        expect(repositoryMock.count).toHaveBeenCalled();
      }
    });

    it('calls given next() if entity found and checkExistenceOnly enabled', async () => {
      middleware = fakeService.createFindByMiddleware({
        checkExistenceOnly: true,
        parameterName,
        reqPropertyName,
      });
      req.params.companyId = faker.random.uuid();
      jest.spyOn(fakeService, 'isExisting').mockResolvedValue(true as any);
      expect.assertions(1);

      await middleware(req, res, next);
      expect(next).toHaveBeenCalledTimes(1);
    });

    it('does not throw when no entity is found and throwIfNotFound disabled', async () => {
      middleware = fakeService.createFindByMiddleware({
        parameterName,
        reqPropertyName,
        throwIfNotFound: false,
      });
      req.params.companyId = faker.random.uuid();

      await middleware(req, res, next);
      expect(repositoryMock.findOne).toHaveBeenCalled();
      expect(next).toHaveBeenCalledTimes(1);
    });

    it('set reqPropertyName in given req with the entity found and call next', async () => {
      middleware = fakeService.createFindByMiddleware({
        parameterName,
        reqPropertyName,
      });
      req.params.companyId = faker.random.uuid();

      const entity: FakeEntity = {} as any;
      jest.spyOn(fakeService, 'findOne').mockResolvedValue(entity as any);
      repositoryMock.findOne = jest.fn().mockResolvedValue(entity);

      await middleware(req, res, next);
      expect(fakeService.findOne).toHaveBeenCalled();
      expect(req.api[reqPropertyName]).toBe(entity);
      expect(next).toHaveBeenCalledTimes(1);
    });
  });

  describe('optionsToTypeOrmOptions', () => {
    it('transforms the given relations', () => {
      const relations: RelationsMap<FakeEntity> = {
        address: true,
        parent: true,
      };
      const options = { relations };

      const result = fakeServiceAsAny.optionsToTypeOrmOptions(options);
      expect(result.relations).toEqual(['address', 'parent']);
    });
  });

  describe('getPrimaryIdNameOrThrow', () => {
    it('throws when no metadata or no primary uuid column is found', () => {
      jest.spyOn(fakeService, 'getFromMetadata' as any).mockReturnValue([]);
      expect(() => (fakeService as any).getPrimaryIdNameOrThrow()).toThrow(/primary id column/i);
    });

    it('returns the propertyName of the first primary uuid column', () => {
      const target = FakeEntity;
      const columnMetadatas: ColumnMetadata[] = [
        {
          target,
          type: 'boolean',
          propertyName: faker.database.column(),
        },
        {
          target,
          type: 'uuid',
          propertyName: faker.database.column(),
        },
        {
          target,
          type: 'number',
          propertyName: faker.database.column(),
        },
        {
          target,
          type: 'uuid',
          propertyName: faker.database.column(),
        },
      ] as any;

      jest.spyOn(fakeService, 'getFromMetadata' as any).mockReturnValue(columnMetadatas);
      const primaryUuidName = (fakeService as any).getPrimaryIdNameOrThrow();
      expect(primaryUuidName).toBe(columnMetadatas[0].propertyName);
    });
  });

  describe('findOneOrFail', () => {
    it('calls repository.findOneOrFail with the given options and returns its result', async () => {
      const resultFromRepo = {};
      repositoryMock.findOneOrFail = jest.fn().mockResolvedValue(resultFromRepo);
      const result = await fakeService.findOneOrFail(findOptions);
      expect(repositoryMock.findOneOrFail).toHaveBeenCalledWith(findOptions);
      expect(resultFromRepo).toBe(result);
    });
  });

  describe('findOneByIdOrFail', () => {
    it('calls repository.findOneByIdOrFail with the given id and returns its value', async () => {
      const resultFromRepo = {};
      repositoryMock.findOneOrFail = jest.fn().mockResolvedValue(resultFromRepo);
      const result = await fakeService.findOneByIdOrFail(id);
      expect(repositoryMock.findOneOrFail).toHaveBeenCalledWith(id, undefined);
      expect(resultFromRepo).toBe(result);
    });

    it('calls repository.findOneByIdOrFail with the given id and options', async () => {
      const resultFromRepo = {};
      repositoryMock.findOneOrFail = jest.fn().mockResolvedValue(resultFromRepo);

      const result = await fakeService.findOneByIdOrFail(id, findOptions);
      expect(repositoryMock.findOneOrFail).toHaveBeenCalledWith(id, findOptions);
      expect(result).toBe(resultFromRepo);
    });
  });
});
