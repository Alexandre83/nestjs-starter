import { mixin, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import _ from 'lodash';
import {
  DeleteResult,
  FindConditions,
  FindManyOptions,
  FindOneOptions,
  InsertResult,
  RemoveOptions,
  Repository,
  SaveOptions,
  UpdateResult,
} from 'typeorm';
import { ColumnMetadata } from 'typeorm/metadata/ColumnMetadata';

import { KeyMap } from '../../definitions/key-map';
import { IApiRequest } from '../../interface/api-request.interface';
import { IFindManyOptions, IFindOneOptions, NonEntityProps, RelationsMap, WhereClause } from '../entity-query.definition';
import { getRelationsPaths } from '../get-relation-paths';

// tslint:disable:member-ordering
/**
 * @example
 *
 * ```
 * // with all parameters (even if all optional)
 * const opts: IFindByMiddlewareOptions<Company> = {
 *   parameterName: 'companyId',
 *   reqPropertyName: 'company',
 *   relations: {
 *     ape: true,
 *     medicalCenter: { address: true },
 *   },
 *   throwIfNotFound: false,
 * };
 * ```
 */
export interface IFindByMiddlewareOptions<Entity> {
  /** if `true`, simply do a `count > 0` check: throw if `count === 0`, or continue  */
  checkExistenceOnly?: boolean;
  /** e.g. `companyId` in route /company/:companyId/contacts */
  parameterName?: string;
  /** if provided, used instead of Entity's first primary uuid column */
  queryField?: NonEntityProps<Entity>;
  /** e.g. { ape: true }, to load `ape` from the relations of the matching entity */
  relations?: RelationsMap<Entity>;
  /** e.g. `company` to create the property `req.gojob.company` */
  reqPropertyName?: string;
  /** if the middleware should throw a 404 error when no entity is found */
  throwIfNotFound?: boolean;
}

export interface IDatabaseService<T> {
  /**
   * Repository of `T`
   */
  repository: Repository<T>;

  /**
   * Build an instance of `T` based on an item (POJO) obtained
   * from a raw SQL query.
   *
   * @param tableAlias Alias of the table in the raw query.
   * @param rawItem Result of the raw query.
   *
   * @return A partial entity made from data (columns/values) available in the raw item.
   */
  buildEntityFromRawResult(rawItem: any, tableAlias: string): Partial<T>;

  /**
   * Count the entities. If any options are given, only the matching entities
   * are counted.
   *
   * @param opts Specify any useful options for the filtering prior counting.
   *
   * @return Resolve with the number of matching entities.
   */
  count(opts?: IFindManyOptions<T>): Promise<number>;

  /**
   * Create a functional middleware responsible for checking that an entity exists,
   * and if it does, adding it to the request in `req.gojob` in the property
   * `options.reqPropertyName` (defaults to the entity name, with first letter
   * lower-cased; e.g. `JobDescription` => `jobDescription`).
   *
   * By default, if `options.parameterName`  (or, by default, the name of the
   * entity's primary uuid column) is not found as a property in the request params,
   * the middleware execution stops and directly jumps to `next` function.
   *
   * If the entity is requested but not found, an error 404 is thrown (except if
   * `throwIfNotFound` is disabled in the `options`).
   *
   * If `checkExistenceOnly` is set to `true`, no property is set in `req.gojob`, and
   * the middleware offers two options: jumping to `next` middleware (the entity
   * exists) or throwing a `NotFoundException`.
   *
   * @param options Configuration of the middleware instance.
   *
   * @return An express middleware.
   */
  // tslint:disable-next-line:ban-types
  createFindByMiddleware(options?: IFindByMiddlewareOptions<T>): Function;

  /**
   * Ask the repository to delete the entities matching with
   * the given `where` clause.
   *
   * @param where Where clause - filter entities to be deleted.
   *
   * @return Resolve on success.
   */
  delete(where: WhereClause<T> | string): Promise<DeleteResult>;

  /**
   * Ask the repository to select the first entity matching the
   * given options.
   *
   * @param findOptions Specify any useful options for the selection,
   * including `relations` to be joined.
   *
   * @return Resolve with results (if any) of type `T[]`, or `undefined`.
   *
   * @example
   *
   * ```
   * CompanyService.find({
   *   where: { ape: null },
   *   relations: { address: true, upsale: true }
   * });
   * ```
   */
  find(options: IFindManyOptions<T>): Promise<T[]>;

  /**
   * Ask the repository to return the entities matching with the given `ids`.
   *
   * @param id Target entities identifiers.
   * @param options Optional query options.
   *
   * @return Resolve with the matching entities.
   */
  findByIds(ids: string[] | number[], options?: IFindManyOptions<T>): Promise<T[]>;

  /**
   * Ask the repository to select the first entity matching the
   * given options.
   *
   * @param options Specify any useful options for the selection,
   * including `relations` to be joined.
   *
   * @return Resolve with an entity (if any) or `undefined`.
   *
   * @example
   *
   * ```
   * CompanyService.findOne({
   *   where: { companyId },
   *   relations: { address: true, upsale: true }
   * });
   * ```
   */
  findOne(options: IFindOneOptions<T>): Promise<T | undefined>;

  /**
   * Ask the repository to return the entity matching with the given `id`.
   *
   * @param id Target entity identifier.
   * @param options Optional query options.
   *
   * @return Resolve with the matching entity, or `undefined`.
   *
   * @example
   *
   * ```
   * const companyId = 'f8e04278-14aa-4c65-a56d-5b847d4badb9';
   * CompanyService.findOneById(companyId);
   * CompanyService.findOneById(companyId, { relations: { address: true } });
   * ```
   */
  findOneById(id: string | number, options?: IFindOneOptions<T>): Promise<T | undefined>;

  /**
   * Same as `findOneById` but throw if no matching result is found.
   *
   * @param id Target entity identifier.
   * @param options Optional query options.
   *
   * @return Resolve with the matching entity or throws.
   *
   * @see DatabaseService#findOneById
   * @throws {Error} No entity could be found, or an unexpected error occurred.
   */
  findOneByIdOrFail(id: string | number, options?: IFindOneOptions<T>): Promise<T>;

  /**
   * Same as `findOneById` but throw if no matching result is found.
   *
   * @param options Optional query options.
   *
   * @return Resolve with the matching entity or throws.
   *
   * @see DatabaseService#findOneById
   * @throws {Error} No entity could be found, or an unexpected error occurred.
   */
  findOneOrFail(options?: IFindOneOptions<T>): Promise<T>;

  /**
   * Same as `findOneById` but throw if no matching result is found.
   *
   * @param id Target entity identifier.
   * @param options Optional query options.
   *
   * @return Resolve with the matching entity or throws.
   *
   * @see DatabaseService#findOneById
   * @throws {Error} No entity could be found, or an unexpected error occurred.
   */
  findOneOrFail(id: string, options?: IFindOneOptions<T>): Promise<T>;

  /**
   * Same as `findOneById` but throw if no matching result is found.
   *
   * @param id Target entity identifier.
   * @param options Optional query options.
   *
   * @return Resolve with the matching entity or throws.
   *
   * @see DatabaseService#findOneById
   * @throws {Error} No entity could be found, or an unexpected error occurred.
   */
  findOneOrFail(id: string, options?: IFindOneOptions<T>): Promise<T>;

  /**
   * Returns a hashmap between T properties and database field name prefixed by their alias or class name
   *
   * @example
   *
   * ```
   * const mapping = getEntityQueryMapping();
   * const mapping = getEntityQueryMapping('usr');
   * ```
   *
   * @param alias
   * @return KeyMap<T, string> Mapping associating class properties to its name in a raw postgresql query
   */
  getEntityQueryMapping(alias?: string): KeyMap<T, string>;

  /**
   * Ask the repository to execute an Insert query with the given
   * set of data `insert` (complete or partial entity).
   *
   * @param insert Data to be inserted in the current table.
   *
   * @return Resolves with the result of the operation, or rejects.
   */
  insert(insert: Partial<T>): Promise<InsertResult>;

  /**
   * Allow determining whether one (or more) entity is matching with the given
   * `where` clause. Sort of convenient shortcut for `count() > 0`.
   *
   * @param where Where clause to filter the entities.
   *
   * @return Resolve with `true` if at least 1 entity match the given `where` clause.
   *
   * @example
   *
   * ```
   * const isCompanyExisting = await companyService.isExisting({ name: 'Gojob' });
   * ```
   */
  isExisting(where: WhereClause<T>): Promise<boolean>;

  /**
   * Ask the repository to remove the entities matching with a given entity
   *
   * @param entity
   * @param options
   *
   * @return Resolves with the result of the operation, or rejects.
   */
  remove(entity: T, options?: RemoveOptions): Promise<T>;

  /**
   * Ask the repository to upsert the given entity(ies).
   *
   * @param entities T(ies) to be inserted (if not existing) or updated.
   * @param [options] options to make a custom saving
   *
   * @return Resolve with upserted result(s).
   */
  save(entityOrEntities: Partial<T> | Array<Partial<T>>, options?: SaveOptions): Promise<T>;

  /**
   * Ask the repository to execute an Update query on the entities
   * matching with the given `where` clause.
   *
   * @param where  Where clause - filter entities to be updated.
   * @param update Update to be applied on matching entities.
   *
   * @return Resolves with the result of the operation, or rejects.
   */
  update(where: WhereClause<T> | string, update: Partial<T>): UpdateResult;
}

export function DatabaseService<T>(entityClass: new () => T, connection?: string): new (...args: any[]) => IDatabaseService<T> {
  class CommonDatabaseService {
    @InjectRepository(entityClass, connection) private repository: Repository<T>;

    buildEntityFromRawResult(rawItem: any, tableAlias: string): Partial<T> {
      const ownColumns = this.getFromMetadata<ColumnMetadata[]>('ownColumns', []);
      const entity: Partial<T> & { [k: string]: any } = {};

      return ownColumns.reduce((acc, col) => {
        const rawColName = `${tableAlias}_${col.databaseName}`;

        if (rawItem[rawColName] !== undefined) {
          acc[col.propertyName] = rawItem[rawColName];
        }

        return acc;
      }, entity);
    }

    count(opts?: IFindManyOptions<T>) {
      return this.repository.count(this.optionsToTypeOrmOptions(opts));
    }

    // tslint:disable-next-line:ban-types
    createFindByMiddleware(options: IFindByMiddlewareOptions<T> = {}): Function {
      return async (
        req: IApiRequest,
        res: Response,
        // tslint:disable-next-line:ban-types
        next?: Function,
      ) => {
        const { checkExistenceOnly, relations, queryField } = options;
        let { parameterName, reqPropertyName } = options;

        const throwNotFound = () => {
          throw new NotFoundException('common:error:entityNotFound');
        };

        const idPrimaryName = this.getPrimaryIdNameOrThrow();
        parameterName = parameterName || idPrimaryName;

        const parameterValue: string = _.get(req, `params.${parameterName}`);
        if (parameterValue === undefined) {
          return typeof next === 'function' && next();
        }

        const where: WhereClause<T> = {
          [queryField || idPrimaryName]: parameterValue,
        } as any;
        if (checkExistenceOnly) {
          const isExisting = await this.isExisting(where);
          return isExisting ? typeof next === 'function' && next() : throwNotFound();
        }

        const entity = await this.findOne({ where, relations });
        if (!entity && options.throwIfNotFound !== false) {
          return throwNotFound();
        }

        // if no "reqProperty" defined, guess the property name to create in `req`
        if (!reqPropertyName) {
          const entityName = this.getFromMetadata<string>('name');
          reqPropertyName = entityName.charAt(0).toLowerCase() + entityName.slice(1);
        }

        if (!req.api) {
          req.api = {};
        }

        req.api[reqPropertyName] = entity;

        if (typeof next === 'function') {
          next();
        }
      };
    }

    delete(where: WhereClause<T> | string) {
      return this.repository.delete(where as FindConditions<T> | string);
    }

    find(options: IFindManyOptions<T>): Promise<T[]> {
      return this.repository.find(this.optionsToTypeOrmOptions(options));
    }

    findByIds(ids: string[] | number[], options?: IFindManyOptions<T>): Promise<T[]> {
      return this.repository.findByIds(ids, this.optionsToTypeOrmOptions(options));
    }

    findOne(options: IFindOneOptions<T>): Promise<T | undefined> {
      return this.repository.findOne(this.optionsToTypeOrmOptions(options));
    }

    findOneById(id: string | number, options?: IFindOneOptions<T>): Promise<T | undefined> {
      return this.repository.findOne(id, this.optionsToTypeOrmOptions(options));
    }

    findOneByIdOrFail(id: string | number, options?: IFindOneOptions<T>): Promise<T> {
      return this.repository.findOneOrFail(id, this.optionsToTypeOrmOptions(options));
    }

    findOneOrFail(options?: IFindOneOptions<T>): Promise<T> {
      return this.repository.findOneOrFail(this.optionsToTypeOrmOptions(options));
    }

    getEntityQueryMapping(alias?: string): KeyMap<T, string> {
      const result: Partial<KeyMap<T, string>> = {};

      const ownColumns = this.getFromMetadata<ColumnMetadata[]>('ownColumns', []);

      ownColumns.forEach(item => {
        result[item.propertyName as keyof T] = (alias || this.getFromMetadata<string>('name')) + '_' + item.databaseName;
      });

      return result as KeyMap<T, string>;
    }

    insert(insert: Partial<T>) {
      return this.repository.insert(insert as any);
    }

    async isExisting(where: WhereClause<T>): Promise<boolean> {
      const count = await this.repository.count({ where });
      return count > 0;
    }

    remove(entity: T, options?: RemoveOptions): Promise<T> {
      return this.repository.remove(entity, options);
    }

    save(entityOrEntities: Partial<T> | Array<Partial<T>>, options?: SaveOptions) {
      // UPDATE_CHECK: "as any" should be removed when TS 3.6 arrives with a fix: see TypeScript/issues/21592
      return this.repository.save(entityOrEntities as any, options);
    }

    update(where: WhereClause<T> | string, update: Partial<T>) {
      return this.repository.update(where as FindConditions<T> | string, update as any);
    }

    /**
     * Get a data of type `U` from the current repository `EntityMetadata`.
     *
     * @param path Path to the data to be obtained (e.g. 'ownColumns)
     * @param defaultValue (optional) Value to be returned if none is available.
     *
     * @return A value of type `U` (if existing), or `defaultValue` (if given) or `undefined`.
     */
    private getFromMetadata<U>(path: string, defaultValue?: U): U {
      return _.get(this.repository, `metadata.${path}`, defaultValue);
    }

    /**
     * Get the name of the first primary column available in the repository `EntityMetadata`,
     * or throw if none is found.
     *
     * @throws {TypeError} When no primary column is declared.
     *
     * @return The name of the first primary column found.
     */
    private getPrimaryIdNameOrThrow(): string {
      const idPrimaryColumn = this.getFromMetadata<ColumnMetadata[]>('primaryColumns', [])[0];

      if (!idPrimaryColumn) {
        throw new TypeError('Invalid use of FindByMiddleware, no primary id column found in metadata');
      }

      return idPrimaryColumn.propertyName;
    }

    /**
     * Transform the custom options as defined by `IFindOneOptions` and
     * `IFindManyOptions` into TypeORM-compatible options.
     * For instance, `relations` have to be converted from a RelationsMap
     * to an array of string (paths to relations to be populated).
     *
     * @param options Enhanced/customized options.
     *
     * @return Options properly converted to TypeORM-compatible options.
     */
    private optionsToTypeOrmOptions(
      options: IFindOneOptions<T> | IFindManyOptions<T> | undefined,
    ): FindOneOptions<T> | FindManyOptions<T> | undefined {
      if (!options) {
        return undefined;
      }

      return {
        ...options,
        order: options.order as { [P in keyof T]?: 'ASC' | 'DESC' | 1 | -1 },
        relations: getRelationsPaths(options.relations),
      };
    }
  }

  return mixin(CommonDatabaseService);
}
