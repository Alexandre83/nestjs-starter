import { Request } from 'express';

export interface IApiRequest<AM = any> extends Request {
  api: {
    [key: string]: any;
  };
}
