import { ExecutionContext, UnauthorizedException } from '@nestjs/common';

import * as bearerGuard from './bearer-auth.guard';

import { User } from '../../modules/user/user.entity';

describe('BearerAuthGuard', () => {
  let guard: bearerGuard.BearerAuthGuard;

  beforeEach(() => {
    guard = new bearerGuard.BearerAuthGuard();
  });

  describe('canActivate', () => {
    let context: ExecutionContext;
    let request: any;
    let response: any;
    let user: any;
    let spyHandleRequest: jest.SpyInstance;
    let spyCreatePassportContext: jest.SpyInstance;
    let mockPassportFn: jest.Mock;

    beforeEach(() => {
      user = { isAuthenticated: true };
      request = { headers: {} };
      response = { body: {} };
      context = {
        switchToHttp: jest.fn().mockReturnThis(),
        getRequest: jest.fn().mockReturnValue(request),
        getResponse: jest.fn().mockReturnValue(response),
      } as any;
      mockPassportFn = jest.fn().mockImplementation((options, cb) => {
        cb();
        return user;
      });
      spyHandleRequest = jest.spyOn(guard, 'handleRequest');
      spyHandleRequest.mockResolvedValue(user);
      spyCreatePassportContext = jest.spyOn(bearerGuard, 'createPassportContext');
      spyCreatePassportContext.mockReturnValue(mockPassportFn);
    });

    afterEach(() => {
      spyHandleRequest.mockRestore();
      spyCreatePassportContext.mockRestore();
    });

    it('should create a passport context function', async () => {
      await guard.canActivate(context);

      expect(spyCreatePassportContext).toHaveBeenCalledWith(request, response);
    });

    it('should execute the callback passed in built function and handle request, depending of result', async () => {
      await guard.canActivate(context);

      expect(mockPassportFn).toHaveBeenCalledWith({ session: false }, expect.any(Function));
      expect(spyHandleRequest).toHaveBeenCalled();
    });

    it('should assign the authenticated user to request ', async () => {
      const result = await guard.canActivate(context);

      expect(request.api.auth).toEqual(user);
      expect(result).toBeTruthy();
    });
  });

  describe('handleRequest', () => {
    let err: any;
    let user: User;
    let isAuthenticated: boolean;

    beforeEach(() => {
      err = undefined;
      user = { userId: 124 } as any;
      isAuthenticated = true;
    });

    it('should throw UnauthorizedException if error occured during request', () => {
      err = { msg: 'test' };

      expect.assertions(2);
      try {
        guard.handleRequest(err, user, isAuthenticated);
      } catch (e) {
        expect(e).toBeInstanceOf(UnauthorizedException);
        expect(e.message.message).toBe('Token expired or not corresponding to an user');
      }
    });

    it('should throw UnauthorizedException if no user found during request', () => {
      user = undefined as any;

      expect.assertions(2);
      try {
        guard.handleRequest(err, user, isAuthenticated);
      } catch (e) {
        expect(e).toBeInstanceOf(UnauthorizedException);
        expect(e.message.message).toBe('Token expired or not corresponding to an user');
      }
    });

    it('should return user and the authentication state', () => {
      const result = guard.handleRequest(err, user, isAuthenticated);

      expect(result).toEqual({ user, isAuthenticated });
    });
  });
});
