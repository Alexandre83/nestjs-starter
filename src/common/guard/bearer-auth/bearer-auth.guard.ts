import { CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import passport, { AuthenticateOptions } from 'passport';

import { User } from './../../modules/user/user.entity';

export class BearerAuthGuard implements CanActivate {
  /**
   * This method determines if the current request can access to last middleware (route endpoint) or not
   * Using passport as promise, it checks if token passed in `bearer` style corresponding to accepted user or not
   *
   * If user is authenticated, the request object will contain it in `api.user` property
   *
   * @param context - context of execution (contains notably the request object)
   *
   * @returns Resolves with true, in case of good authentication
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const [request, response] = [context.switchToHttp().getRequest(), context.switchToHttp().getResponse()];
    const passportFn = createPassportContext(request, response);
    const authenticationResult = await passportFn({ session: false }, (err: any, user: any, info: any) => this.handleRequest(err, user, info));

    request.api = { ...request.api, auth: authenticationResult };
    return true;
  }

  /**
   * Handle the request after passport validation
   *
   * @throws with UnauthorizedException when no user
   * @throws with UnauthorizedException when error occured
   *
   * @param err - error occured during passport validation
   * @param user - user authenticated or undefined
   * @param isAuthenticated - the status of authentication for this user
   *
   * @returns an object containing `user` and `isAuthenticated`
   */
  handleRequest(err: any, user: User | undefined, isAuthenticated: boolean): { isAuthenticated: boolean; user: User } {
    if (err || !user) {
      throw new UnauthorizedException('Token expired or not corresponding to an user');
    }
    return { user, isAuthenticated };
  }
}

/**
 * Create a passport context that return a function to execute authenticate of passport module
 * It returns a promise an using a callback function that will be executed after a good authentication
 *
 * @param request
 * @param response
 *
 * @throws When passport.authenticate returns an error
 *
 * @returns a function that will return a promise of passport authenticate
 */
// tslint:disable-next-line:ban-types
export const createPassportContext = (request: any, response: any) => (options: AuthenticateOptions, callback: Function) =>
  new Promise((resolve, reject) =>
    passport.authenticate('bearer', options, (err: any, user, info) => {
      try {
        return resolve(callback(err, user, info));
      } catch (err) {
        reject(err);
      }
    })(request, response, (err: any) => (err ? reject(err) : resolve)),
  );
