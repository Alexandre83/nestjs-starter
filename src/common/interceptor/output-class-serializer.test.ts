import { ClassSerializerInterceptor, Type as ClassType } from '@nestjs/common';
import { ClassTransformOptions, Expose, Type } from 'class-transformer';

import * as snv from '../tools/strip-null-values';
import { OutputClassSerializer } from './output-class-serializer';

// tslint:disable:max-classes-per-file
describe('OutputClassSerializer', () => {
  it('returns a class extending ClassSerializerInterceptor', () => {
    const result = OutputClassSerializer(class FakeOutDto {});

    expect(result).toBeInstanceOf(Function);
    expect(result.prototype.intercept).toBeDefined();
    expect(result.prototype.intercept).toBeDefined();
    expect(new result()).toBeInstanceOf(ClassSerializerInterceptor);
  });

  describe('OutputClassSerializer (mixin)', () => {
    let getMixinClass: (serializedClass?: ClassType<any>, options?: any) => ClassType<ClassSerializerInterceptor>;

    beforeEach(() => {
      getMixinClass = (serializedClass = class {}, options = {}) => OutputClassSerializer(serializedClass, options);
    });

    describe('transformToPlain', () => {
      let spySuperCall: jest.SpyInstance;
      let serializedClass: ClassType<any>;
      let defaultSerializer: ClassSerializerInterceptor;
      let mutationOptions: any;
      let serializedValue: any;
      let transformOptions: ClassTransformOptions;

      let spyStripNull: jest.SpyInstance;

      beforeEach(() => {
        spySuperCall = jest.spyOn(ClassSerializerInterceptor.prototype, 'transformToPlain');

        serializedClass = class {};
        mutationOptions = { stripNullProps: false };
        serializedValue = { field: 'value' };
        transformOptions = {};

        spyStripNull = jest.spyOn(snv, 'stripNullValues');

        defaultSerializer = new (getMixinClass(serializedClass, mutationOptions))();
      });

      afterEach(() => {
        spySuperCall.mockRestore();
        spyStripNull.mockRestore();
      });

      describe('Serialization', () => {
        it('assigns the serialized value to an instance of the serialized class before calling super method', () => {
          defaultSerializer.transformToPlain(serializedValue, transformOptions);
          expect(spySuperCall).toHaveBeenCalledWith(Object.assign(new serializedClass(), serializedValue), transformOptions);
        });

        it('uses "excludeAll" as a transform strategy, if none is given', () => {
          const options: ClassTransformOptions = { enableCircularCheck: false };
          defaultSerializer.transformToPlain(serializedValue, options);

          expect(spySuperCall).toHaveBeenCalledWith(expect.anything(), options);
          expect(options.strategy).toBe('excludeAll');
        });

        it('use the given strategy', () => {
          const strategy = 'exposeAll';
          transformOptions.strategy = strategy;

          defaultSerializer.transformToPlain(serializedValue, transformOptions);
          // ensure it did not change
          expect(transformOptions.strategy).toBe(strategy);
        });
      });

      describe('Null values stripping', () => {
        it('strips null properties if stripNullProps is true when creating the mixin class', () => {
          const mutatedResult = { props: 'notNullLol' };
          spyStripNull.mockReturnValue(mutatedResult);

          const strippingSerializer = new (getMixinClass(serializedClass, { stripNullProps: true }))();
          strippingSerializer.transformToPlain(serializedValue, transformOptions);

          expect(spySuperCall).toHaveBeenCalledWith(Object.assign(new serializedClass(), mutatedResult), transformOptions);
          expect(spyStripNull).toHaveBeenCalledWith(serializedValue);
        });

        it('does no stripping if not asked', () => {
          defaultSerializer.transformToPlain(serializedValue, transformOptions);
          expect(spyStripNull).not.toHaveBeenCalled();
        });
      });

      describe('Keep unchanged properties', () => {
        class BadGirl {
          @Expose()
          firstName: string;
        }

        class BadGuy {
          @Expose()
          answer: number;

          @Expose()
          @Type(() => BadGirl)
          girlFriend: BadGirl;

          @Expose()
          pokemons: object[];
        }

        let inputInstance: BadGuy;

        beforeEach(() => {
          inputInstance = new BadGuy();
          inputInstance.answer = 42;
          inputInstance.girlFriend = { firstName: 'Kim' };
          inputInstance.pokemons = [{ id: 1 }, { id: 2 }];
        });

        it('Without "keepUnchangedProperties", POJOs in array of objects are all emptied', () => {
          const customSerializer = new (getMixinClass(BadGuy))();
          const result = customSerializer.transformToPlain(inputInstance, {});
          expect(result).toEqual({ ...inputInstance, pokemons: [{}, {}] } as BadGuy);
        });

        it('With "keepUnchangedProperties", array of POJOs can be preserved', () => {
          const customSerializer = new (getMixinClass(BadGuy, { keepUnchangedProps: ['pokemons'] }))();
          const result = customSerializer.transformToPlain(inputInstance, {});
          expect(result).toEqual(inputInstance);
        });
      });
    });
  });
});
