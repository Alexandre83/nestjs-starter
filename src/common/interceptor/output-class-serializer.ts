import { ClassSerializerInterceptor, mixin, PlainLiteralObject, Type, UseInterceptors } from '@nestjs/common';
import { ClassTransformOptions } from 'class-transformer';

import { stripNullValues } from '../tools/strip-null-values';
import { pick } from '../tools/utility.tools';

interface IMutationOptions<T> {
  /** properties that should not be transformed/serialized and kept "as is" in the output (response) */
  keepUnchangedProps?: Array<keyof T>;
  /** If set to `false`, then null-props stripping is not used before Serialization */
  stripNullProps?: boolean;
}

/**
 * Creates a mixin class extending the built-in `ClassSerializerInterceptor` (NestJS)
 * in order to:
 * - Embed the `class` type being serialized
 * - Allow enabling/disabling the null-props stripping.
 * - Allow to keep properties "as is" (untouched: not transformed, not serialized, etc)
 *
 * @param serializedClass Class being serialized: supposed to contains decorators from `class-transformer`.
 * @param options Configure the operations made during the pre/post serialization.
 *
 * @return A pre-configured mixin class injectable as a Serializer Interceptor.
 */
export function OutputClassSerializer<T>(serializedClass: Type<T>, options: IMutationOptions<T> = {}): Type<ClassSerializerInterceptor> {
  class CustomSerializer extends ClassSerializerInterceptor {
    transformToPlain(plainOrClass: any, transformOptions: ClassTransformOptions = {}): PlainLiteralObject {
      if (!transformOptions.strategy) {
        transformOptions.strategy = 'excludeAll';
      }

      const { stripNullProps = true } = options || {};
      const initialValue = stripNullProps !== false ? stripNullValues(plainOrClass) : plainOrClass;
      const serializedValue = super.transformToPlain(Object.assign(new serializedClass(), initialValue), transformOptions);

      if (!Array.isArray(options.keepUnchangedProps)) {
        return serializedValue;
      }

      return {
        ...serializedValue,
        ...pick(initialValue, ...options.keepUnchangedProps),
      };
    }
  }

  return mixin(CustomSerializer);
}

/**
 * Shortcut decorator to use output serialization.
 *
 * @param dto Output Dto class used for serialization.
 * @param options Options applied before the serialization.
 *
 * @return `UseInterceptors(OutputClassSerializer(dto, options))`.
 */
export function Output<T>(dto: Type<T>, options: IMutationOptions<T> = {}) {
  return UseInterceptors(OutputClassSerializer(dto, options));
}
