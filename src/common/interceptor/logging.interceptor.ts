import { CallHandler, ExecutionContext, Inject, Injectable, LoggerService, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(@Inject('Logger') private readonly logger: LoggerService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const { headers, body, ip, method, url } = context.switchToHttp().getRequest();

    this.logger.log(`[ROUTE] ${method.toUpperCase()} ${url} from ${ip} with token ${headers.authorization} and body: ${JSON.stringify(body)}`);
    return next.handle();
  }
}
