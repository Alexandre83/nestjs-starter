import { Logger as NestLogger, LoggerService } from '@nestjs/common';
import { createLogger, Logger } from 'winston';
import { Console } from 'winston/lib/winston/transports';

import { configService } from '../../config/config.service';

export class ApiLogger implements LoggerService {
  constructor() {
    // const env = configService.getString('NODE_ENV')!;
    this.logger = createLogger({
      transports: [new Console()],
    });
  }

  private logger: Logger;

  error(message: any, trace?: string | undefined, context?: string | object | undefined) {
    this.logger.error(message, { trace, context });
  }

  log(message: any, context?: string | object | undefined) {
    this.logger.info(message, { context });
  }

  warn(message: any, context?: string | object | undefined) {
    this.logger.warn(message, { context });
  }
}

export const LoggerProvider = {
  provide: 'Logger',
  useClass: configService.getString('NODE_ENV') === 'production' ? ApiLogger : NestLogger,
};
