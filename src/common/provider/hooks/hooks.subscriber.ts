import { Injectable } from '@nestjs/common';
import { Controller } from '@nestjs/common/interfaces';
import { ModulesContainer } from '@nestjs/core';
import { InstanceWrapper } from '@nestjs/core/injector/instance-wrapper';
import { Module } from '@nestjs/core/injector/module';
import { MetadataScanner } from '@nestjs/core/metadata-scanner';
import _ from 'lodash';

import { IHookConfig } from '../../decorator/api-hooks.decorator';
import { IDatabaseService } from '../../tools/database-service/database-service';

@Injectable()
export class HooksSubscriberExplorer {
  constructor(private readonly modulesContainer: ModulesContainer, private readonly metadataScanner: MetadataScanner) {}

  public explore(): Array<IHookConfig<any>> {
    const modules = [...this.modulesContainer.values()];
    const controllersMap = modules.filter(({ controllers }) => controllers.size > 0).map(({ controllers }) => controllers);

    const instanceWrappers: Array<InstanceWrapper<Controller> | undefined> = [];
    controllersMap.forEach(map => {
      const mapKeys = [...map.keys()];
      instanceWrappers.push(
        ...mapKeys.map(key => {
          return map.get(key);
        }),
      );
    });

    const sortedInstanceWrappers: Array<InstanceWrapper<Controller>> = _.compact(instanceWrappers);

    const hooksConfig = sortedInstanceWrappers
      .map(({ instance }) => {
        const instancePrototype = Object.getPrototypeOf(instance);
        return this.metadataScanner.scanFromPrototype(instance, instancePrototype, method =>
          this.exploreMethodMetadata(instance, instancePrototype, method),
        );
      })
      .reduce((prev, curr) => {
        return prev.concat(curr);
      });

    return _.compact(hooksConfig);
  }

  public exploreMethodMetadata(instance: object, instancePrototype: Controller, methodKey: string): IHookConfig<any> | null {
    const targetCallback = (instancePrototype as any)[methodKey];
    const handler = Reflect.getMetadata('HOOKS_SUBSCRIBER', targetCallback);
    return handler;
  }

  public getInstance<T>(providerName: string) {
    const providers = [...this.modulesContainer.values()]
      .map((module: Module) => {
        return [...module.providers.values()];
      })
      .reduce((a, b) => a.concat(b), []);

    const service: T[] = providers.map(provider => {
      const { instance } = provider;

      if (instance && instance.constructor && instance.constructor.name === providerName) {
        return Object.create(instance);
      }
    });

    const services = _.compact(service);

    return services[0];
  }

  public getProviderInstance<T>(providerName: string) {
    const providers = [...this.modulesContainer.values()]
      .map((module: Module) => {
        return [...module.providers.values()];
      })
      .reduce((a, b) => a.concat(b), []);

    const service: Array<IDatabaseService<T>> = providers.map(provider => {
      const { instance } = provider;

      if (instance && instance.constructor && instance.constructor.name === providerName) {
        return Object.create(instance);
      }
    });

    const services = _.compact(service);

    return services[0];
  }
}
