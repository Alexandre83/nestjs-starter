import { Global, Module } from '@nestjs/common';
import { MetadataScanner } from '@nestjs/core/metadata-scanner';

import { HooksService } from './hooks.service';
import { HooksSubscriberExplorer } from './hooks.subscriber';

@Global()
@Module({
  imports: [],
  providers: [MetadataScanner, HooksSubscriberExplorer, HooksService],
  exports: [HooksService],
})
export class HooksModule {}
