import { Injectable } from '@nestjs/common';
import _ from 'lodash';

import { IDatabaseService } from '../../tools/database-service/database-service';
import { Anchors, HookCallback, IHookConfig } from './../../decorator/api-hooks.decorator';
import { HooksSubscriberExplorer } from './hooks.subscriber';

interface IHookServiceItems<T> {
  afterDelete: HookCallback[];
  afterInsert: HookCallback[];
  afterSelect: HookCallback[];
  afterUpdate: HookCallback[];
  beforeDelete: HookCallback[];
  beforeInsert: HookCallback[];
  beforeSelect: HookCallback[];
  beforeUpdate: HookCallback[];
  service?: IDatabaseService<T>;
}

interface IHooks {
  [key: string]: IHookServiceItems<any>;
}

@Injectable()
export class HooksService {
  constructor(private readonly hookExplorer: HooksSubscriberExplorer) {
    const hooksConfig: Array<IHookConfig<any>> = this.hookExplorer.explore();
    this.dispatchHooks(hooksConfig);
  }

  private hooks: IHooks = {};

  public count(service: string, anchor: Anchors): number {
    return this.hooks[service][anchor].length;
  }

  public getInstance<T>(service: string): T {
    return this.hookExplorer.getInstance<T>(service);
  }

  public getServiceInstance<T>(service: string): IDatabaseService<T> {
    if (!this.hooks[service]) {
      this.defineHook(service);
    }

    let instance: IDatabaseService<T> | undefined = this.hooks[service].service;

    if (!instance) {
      const serviceInstance = this.hookExplorer.getProviderInstance<T>(service);

      if (!serviceInstance) {
        throw new Error(`No provider instance found with name ${service}`);
      }

      this.hooks[service].service = serviceInstance;
      instance = this.hooks[service].service;
    } else {
    }

    return instance!;
  }

  public runHooks<T>(anchor: Anchors, service: string, datas: T) {
    if (this.hooks[service] && this.hooks[service][anchor]) {
      this.hooks[service][anchor].forEach(hook => {
        hook(datas, this);
      });
    }
  }

  private defineHook(service: string) {
    this.hooks[service] = {
      afterDelete: [],
      afterInsert: [],
      afterSelect: [],
      afterUpdate: [],
      beforeDelete: [],
      beforeInsert: [],
      beforeSelect: [],
      beforeUpdate: [],
    };
  }

  private dispatchHooks(hooksConfig: Array<IHookConfig<any>>): void {
    hooksConfig.map(hookConfig => {
      if (!this.hooks[hookConfig.serviceName]) {
        this.defineHook(hookConfig.serviceName);
      }

      hookConfig.anchors.forEach((anchor: Anchors) => {
        this.hooks[hookConfig.serviceName][anchor].push(hookConfig.callback);
      });

      if (!this.hooks[hookConfig.serviceName].service) {
        const service = this.hookExplorer.getProviderInstance(hookConfig.serviceName);

        if (!service) {
          throw new Error(`No provider instance found with name ${service}`);
        } else {
          this.hooks[hookConfig.serviceName].service = service;
        }
      }
    });
  }
}
