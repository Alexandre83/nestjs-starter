import { SetMetadata } from '@nestjs/common';

import { HooksService } from '../provider/hooks/hooks.service';

export type Anchors =
  | 'beforeInsert'
  | 'beforeUpdate'
  | 'beforeDelete'
  | 'beforeSelect'
  | 'afterInsert'
  | 'afterUpdate'
  | 'afterDelete'
  | 'afterSelect';

export const Hook = <T>(service: string, anchors: Anchors[]) => {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    SetMetadata<string, IHookConfig<T>>('HOOKS_SUBSCRIBER', {
      callback: descriptor.value,
      methodName: propertyKey,
      serviceName: service,
      target: target.constructor.name,
      anchors,
    })(target, propertyKey, descriptor);
  };
};

export interface IHookConfig<T> {
  anchors: Anchors[];
  callback: HookCallback;
  methodName?: string;
  serviceName: string;
  target?: string;
}

export type HookCallback = <T>(data: T, hookService: HooksService) => void;
