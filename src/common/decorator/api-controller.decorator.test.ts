import * as NestCommon from '@nestjs/common';
import * as NestSwagger from '@nestjs/swagger';
import { ClassTransformOptions } from 'class-transformer';

import { APIController } from './api-controller.decorator';

describe('APIController decorator', () => {
  let spyApiUseTags: jest.SpyInstance;
  let spyApiBearerAuth: jest.SpyInstance;
  let spySerializeOptions: jest.SpyInstance;
  let spyUsePipes: jest.SpyInstance;
  let spyUseGuards: jest.SpyInstance;
  let decoratorImplem: jest.Mock;

  let target: NestCommon.Type<any>;

  beforeEach(() => {
    decoratorImplem = jest.fn();

    spyApiUseTags = jest.spyOn(NestSwagger, 'ApiUseTags');
    spyApiBearerAuth = jest.spyOn(NestSwagger, 'ApiBearerAuth');
    spySerializeOptions = jest.spyOn(NestCommon, 'SerializeOptions');
    spyUseGuards = jest.spyOn(NestCommon, 'UseGuards');
    spyUsePipes = jest.spyOn(NestCommon, 'UsePipes');

    target = class {};
  });

  afterEach(() => {
    decoratorImplem.mockReset();

    spyApiUseTags.mockRestore();
    spyApiBearerAuth.mockRestore();
    spySerializeOptions.mockRestore();
    spyUseGuards.mockRestore();
    spyUsePipes.mockRestore();
  });

  describe('SerializeOptions', () => {
    beforeEach(() => spySerializeOptions.mockReturnValue(decoratorImplem));

    it('uses default serialize options', () => {
      APIController()(target);

      expect(spySerializeOptions).toHaveBeenCalledWith({ strategy: 'excludeAll' });
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });

    it('uses given serialize options', () => {
      const serializeOptions = { strategy: 'exposeAll' } as ClassTransformOptions;
      APIController('', { serializeOptions })(target);

      expect(spySerializeOptions).toHaveBeenCalledWith(serializeOptions);
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });
  });

  describe('ApiUseTags', () => {
    beforeEach(() => spyApiUseTags.mockReturnValue(decoratorImplem));

    it('does not apply any tags if none given', () => {
      APIController()(target);

      expect(spyApiUseTags).not.toHaveBeenCalled();
      expect(decoratorImplem).not.toHaveBeenCalled();
    });

    it('applies the given tags', () => {
      const tags = ['hello', 'world'];
      const swagger = { tags } as any;
      APIController('', { swagger })(target);

      expect(spyApiUseTags).toHaveBeenCalledWith(...tags);
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });
  });

  describe('ApiBearerAuth', () => {
    beforeEach(() => spyApiBearerAuth.mockReturnValue(decoratorImplem));

    it('applies by default', () => {
      APIController()(target);

      expect(spyApiBearerAuth).toHaveBeenCalled();
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });

    it('applies if option useBearerAuth is true', () => {
      const swagger = { useBearerAuth: true } as any;
      APIController('', { swagger })(target);

      expect(spyApiBearerAuth).toHaveBeenCalled();
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });

    it('does not apply if option useBearerAuth is false', () => {
      const swagger = { useBearerAuth: false } as any;
      APIController('', { swagger })(target);

      expect(spyApiUseTags).not.toHaveBeenCalled();
      expect(decoratorImplem).not.toHaveBeenCalled();
    });
  });

  describe('UsePipes', () => {
    beforeEach(() => spyUsePipes.mockReturnValue(decoratorImplem));

    it('applies by default', () => {
      APIController()(target);

      expect(spyUsePipes.mock.calls[0][0]).toBeInstanceOf(NestCommon.ValidationPipe);
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });

    it('applies if option useValidationPipe is true', () => {
      APIController('', { useValidationPipe: true })(target);

      expect(spyUsePipes.mock.calls[0][0]).toBeInstanceOf(NestCommon.ValidationPipe);
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });

    it('does not apply if option useValidationPipe is false', () => {
      APIController('', { useValidationPipe: false })(target);

      expect(spyUsePipes).not.toHaveBeenCalled();
      expect(decoratorImplem).not.toHaveBeenCalled();
    });
  });

  describe('UseGuards', () => {
    beforeEach(() => spyUseGuards.mockReturnValue(decoratorImplem));

    it('does not apply any tags if none given', () => {
      APIController()(target);

      expect(spyUseGuards).not.toHaveBeenCalled();
      expect(decoratorImplem).not.toHaveBeenCalled();
    });

    it('applies the given tags', () => {
      const guards = [{ value: 'hello' }, { value: 'world' }] as any;
      APIController('', { useGuards: guards })(target);

      expect(spyUseGuards).toHaveBeenCalledWith(...guards);
      expect(decoratorImplem).toHaveBeenCalledWith(target);
    });
  });
});
