import { Controller, SerializeOptions, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { ClassTransformOptions } from 'class-transformer';

export interface IAPIControllerOptions {
  /** Define the `class-transformer` option for interceptor "ClassSerializer" */
  serializeOptions: ClassTransformOptions;
  /** Configuration of `@nestjs/swagger` decorators */
  swagger?: {
    tags?: string[];
    useBearerAuth?: boolean;
  };
  /** Declare some guards */
  useGuards: any[];
  /** Whether the controller should declare `@UsePipes` with a `ValidationPipe` */
  useValidationPipe: boolean;
}

/**
 * High-order decorator which allows applying multiple decorators on a
 * single Controller class, like: Swagger metadata decorators, Serialization
 * options, and enabling/disabling the use of a validation pipe.
 *
 * @param prefixPath Prefix for Route handlers embedded in the decorated Controller class.
 * @param options Configuration - determine which additional decorators are applied on the decorated class.
 */
export function APIController(prefixPath = '/', options: Partial<IAPIControllerOptions> = {}): ClassDecorator {
  return (target: any) => {
    const {
      serializeOptions = { strategy: 'excludeAll' } as ClassTransformOptions,
      swagger = { tags: [], useBearerAuth: true },
      useValidationPipe,
      useGuards = [],
    } = options;

    if (swagger) {
      if (swagger.tags && swagger.tags.length) {
        ApiUseTags(...swagger.tags)(target);
      }
      if (swagger.useBearerAuth) {
        ApiBearerAuth()(target);
      }
    }

    if (useValidationPipe !== false) {
      UsePipes(new ValidationPipe({ whitelist: true }))(target);
    }

    if (useGuards && useGuards.length) {
      UseGuards(...useGuards)(target);
    }

    SerializeOptions(serializeOptions)(target);

    Controller(prefixPath)(target);
  };
}
