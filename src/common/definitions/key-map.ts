/**
 * Return a KeyMap based on a class and a subtype
 *
 * @example
 *
 * ```
 *  class Twitter {
 *    id: string;
 *    msg: string;
 *    read: number;
 *  }
 *
 *  KeyMap<Twitter, string> => { id: string, name: string, read: string }
 * ```
 */

export type KeyMap<T, U> = { [key in keyof T]: U };
