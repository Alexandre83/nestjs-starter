/* tslint:disable */

/**
 * DeepPartial: Infinitely nested `Partial`.
 * Based on TypeORM's `DeepPartial` type, but with support for
 * `Array` and `Date`.
 *
 * `Array`: TypeORM's version is typing array types as `DeepPartial<Something[]>`
 * instead of `DeepPartial<Something>[]`.
 * `Date`: TypeORM's version is typing properties of type `Date` as `DeepPartial<Date>`
 * whereas we want/expect `Date | undefined`.
 */
export type DeepPartial<T> = {
  [P in keyof T]?: T[P] extends Array<infer U> ? Array<DeepPartial<U>> : T[P] extends Date | undefined ? T[P] : DeepPartial<T[P]>
};
