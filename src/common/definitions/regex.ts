export const IS_EMAIL_REGEX = /^((?!\.)[\w-_.]*[^.])(@[\w-_.]+)(\.\w+(\.\w+)?[^.\W])$/;
export const IS_PASSWORD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,32}$/;
