import { getOrDefault } from './../../tools/copy-constructor.tools';

export class User {
  userId: string;

  constructor(copy: Partial<User> = {}) {
    this.userId = getOrDefault(copy.userId, undefined as any);
  }
}
