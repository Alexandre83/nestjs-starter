import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isNil } from '@nestjs/common/utils/shared.utils';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { ValidationError } from 'class-validator/validation/ValidationError';

/**
 * Responsible for validating DTO instances according to the
 * constraints defined in their class.
 *
 * Unexpected properties of dtoInstance (class properties
 * without decorators) are automatically stripped (option `whitelist`).
 *
 * The parameters of type "custom", created with `createParamDecorator`,
 * are ignored by this Pipe, in order not to whitelist-strip the Route Parameters
 * we pass to our controllers (e.g. `@CompanyParam`).
 */
@Injectable()
export class TypedValidationPipe implements PipeTransform<any> {
  forcedType: new (...args: any[]) => any;

  constructor(forcedType: new (...args: any[]) => any) {
    this.forcedType = forcedType;
  }

  async transform(value: any, metadata: ArgumentMetadata) {
    const type = this.forcedType;

    if (!type || !this.shouldBeValidated(type)) {
      return value;
    }

    let dtoInstance: any;
    let errors: ValidationError[];

    try {
      dtoInstance = plainToClass(type, value);
      errors = await validate(dtoInstance, { whitelist: true });
    } catch (err) {
      throw new BadRequestException(err.message || err);
    }

    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }

    return dtoInstance;
  }

  private shouldBeValidated(metatype: any): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find(type => metatype === type) && !isNil(metatype);
  }
}
