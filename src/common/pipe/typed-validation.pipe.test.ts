import { BadRequestException } from '@nestjs/common';
import { IsNumber, IsString } from 'class-validator';
import { DeepPartial } from 'typeorm';

import { TypedValidationPipe } from './typed-validation.pipe';

class FakeEntity {
  created: Date;
  id: string;
  name: string;
  updated: Date;
  value: number;
}

// tslint:disable-next-line:max-classes-per-file
class FakeEntityPostInDto {
  @IsString()
  name: string;

  @IsNumber()
  value: number;
}

describe('TypedValidationPipe', () => {
  let typedValidationPipe: TypedValidationPipe;

  beforeEach(() => {
    typedValidationPipe = new TypedValidationPipe(FakeEntityPostInDto);
  });

  describe('With FakeEntityPostInDto', () => {
    let input: any;

    /**
     * Provide a default template for a (valid) FakeEntityPostInDto input.
     *
     * @return {object} a valid input matching the definition of FakeEntityPostInDto.
     */
    const getFakeEntityInput = () =>
      ({
        name: 'Fake Entity',
        value: 12,
      } as DeepPartial<FakeEntity>);

    beforeEach(() => {
      input = getFakeEntityInput();
    });

    it('strips undesired properties (based on a DTO class definition)', async done => {
      input.notWanted = 'should-be-removed';

      try {
        const entity = await typedValidationPipe.transform(input, undefined as any);
        expect(entity).not.toBeFalsy();
        expect(entity).not.toHaveProperty('notWanted');
        done();
      } catch (e) {
        done.fail(e);
      }
    });

    it('throws a BadRequestException if a desired property is missing', async done => {
      delete input.name;

      try {
        await typedValidationPipe.transform(input, undefined as any);
        fail();
      } catch (e) {
        expect(e).toBeInstanceOf(BadRequestException);
        done();
      }
    });

    it('returns the input if no type is provided', async () => {
      delete typedValidationPipe.forcedType;
      const entity = await typedValidationPipe.transform(input, undefined as any);

      expect(entity).toBe(input);
    });

    it('returns the input if metatype is: primitive, Array, or Object', async () => {
      const types = [String, Boolean, Number, Array, Object];

      for (const type of types) {
        typedValidationPipe.forcedType = type;

        const entity = await typedValidationPipe.transform(input, undefined as any);
        expect(entity).toBe(input);
      }
    });

    it('returns a correctly typed input when input is valid', async done => {
      try {
        const entity = await typedValidationPipe.transform(input, undefined as any);
        expect(entity).toBeInstanceOf(FakeEntityPostInDto);
        expect(entity).not.toBe(input);
        done();
      } catch (e) {
        done.fail(e);
      }
    });
  });
});
