import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv-safe';
import * as path from 'path';

type EnvType =
  | 'PORT'
  | 'NODE_ENV'
  | 'DATABASE_DROP'
  | 'DATABASE_SYNCHRONIZE'
  | 'DATABASE_FIXTURES'
  | 'DATABASE_MIGRATIONS'
  | 'DB_HOST'
  | 'DB_PORT'
  | 'DB_NAME'
  | 'DB_USER'
  | 'DB_PWD'
  | 'DB_SCHEMA';

@Injectable()
export class ConfigService {
  constructor() {
    let filename = '.env';
    if (process.env.NODE_ENV && process.env.NODE_ENV !== 'development') {
      filename = `.env.${process.env.NODE_ENV}`;
    }
    dotenv.config({
      path: path.resolve(process.cwd(), filename),
      allowEmptyValues: true,
    });
  }

  getBoolean(key: EnvType) {
    return !!process.env[key];
  }

  getNumber(key: EnvType) {
    return +(process.env[key] || 0) as number;
  }

  getString(key: EnvType) {
    return process.env[key];
  }
}

export const configService = new ConfigService();
